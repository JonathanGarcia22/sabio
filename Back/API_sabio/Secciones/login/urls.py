from django.urls import path
from rest_framework_simplejwt.views import TokenVerifyView, TokenRefreshView

from .views import Login, ChangePassword, ResetPassword, ResponseResetPassword

urlpatterns = [
    path('', Login.as_view(), name='login'),
    path('change_password/', ChangePassword.as_view(), name='change_password'),
    path('reset_password/', ResetPassword.as_view(), name='reset_password'),
    path('response_reset_password/<uidb64>[0-9A-Za-z_\-]+/<token>/', ResponseResetPassword.as_view(), name='new_reset_password'),
    path('token/verify/', TokenVerifyView.as_view(), name='token_verify'),
    path('token-refresh/', TokenRefreshView.as_view(), name='token_refresh')
]