from django.utils.http import urlsafe_base64_decode, urlsafe_base64_encode
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.contrib.auth import authenticate

from ..usuarios.models import User
#from datetime import datetime
from django.contrib.auth import (get_user_model)
from django.core.exceptions import ValidationError

from django.contrib.auth.tokens import default_token_generator
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import smtplib

from django.utils.encoding import force_bytes
from django.utils import timezone


def get_tokens_for_user(user):
    token = RefreshToken.for_user(user)         #JWT

    firts_Session = False
    if (user.last_login == None):               #¿Primer inicio de sesion?
        print("Primer inicio de sesion")
        firts_Session = True
    else:
        print("No es el primer inicio de sesion")
        user.last_login = timezone.now()

    return {
        'refresh': str(token),
        'access': str(token.access_token),
        'username': user.username,
        'id': str(user.id),
        'first_session': firts_Session,
        'rol': user.rol,
        'entidad': user.entidad,
    }

class Login(APIView):

    def post(self, request):
        query_active = []
        try:
            query_active = User.objects.filter(username=self.request.data['username'])

            if ( query_active[0].is_active ):

                user = authenticate(username=self.request.data['username'], password=self.request.data['password'])     #Valida usuario
                print(user)

                token = get_tokens_for_user(user)

                instance = user
                instance.counter = 0
                instance.save()

                reponse = token

                return Response(reponse, status=status.HTTP_200_OK)

            else:
                print("El usuario se encuentra inactivo")
                return Response({'message': 'El usuario se encuentra inactivo'}, status=status.HTTP_200_OK)

        except:
            if ( len(query_active) > 0):
                # Si el usuario existe, sigue esta linea de flujo y tiene en cuenta el contador
                instance = query_active[0]
                count = instance.counter

                #========Consulta fecha del primer intento fallido y valida si ya puede reiniciar el contador===========
                date_counter = instance.date_counter
                if ( (date_counter != None) and ( count > 0 ) ):
                    delta_time = date_counter + timezone.timedelta(days=1)
                    # Si la suma de la fecha guardada mas un dia es menor a la fecha actual, entonces ya paso más de un día
                    # y el contador aun no se debe reiniciar

                    if (delta_time < timezone.now()):
                        print("Ya paso más de una día")
                        count = 0
                    else:
                        print("Aun no ha pasado más de un día")


                #==================================Aumentar contador y validar==========================================
                count += 1

                #Se guarda el valor de contador
                instance.counter = count
                if (count == 1):            #la fecha se guarda solo si el contador es 1
                    instance.date_counter = timezone.now()
                instance.save()

                if (count < 4):
                    return Response({'Error': 'Credenciales inválidas'}, status=status.HTTP_400_BAD_REQUEST)
                if (count > 3 and count < 6):
                    return Response({'Error': 'Tiene ' + str(
                        6 - count) + ' intentos más y su cuenta sera bloqueada, si olvido su contraseña'
                                     ' dirijase a la pestaña Olvido su clave'}, status=status.HTTP_400_BAD_REQUEST)
                if (count == 6):
                    inactive_user = User.objects.filter(username=self.request.data['username'])
                    instance = inactive_user[0]
                    instance.is_active = False
                    instance.counter = 0
                    instance.save()

                    return Response({'Error': 'Su cuenta ha sido bloqueada, favor comunicarse con el adminstrador'},
                                    status=status.HTTP_400_BAD_REQUEST)
            else:
                #El usuario ingresado no existe y no se tiene en cuenta el contador
                return Response({'Error': 'Credenciales inválidas'}, status=status.HTTP_400_BAD_REQUEST)


class ChangePassword(APIView):

    def post(self, request):
        try:
            query = User.objects.filter(id=self.request.data['id'], username=self.request.data['username'])

            if ( len(query) < 1 ):
                return Response({'error':"Error de autenticación. Por favor logearse de nuevo. Si el problema persiste, "
                                "favor comunicarse con el administrador de la plataforma"},status=status.HTTP_400_BAD_REQUEST)

            if ( self.request.data['password1'] != self.request.data['password2']):
                return Response({"error":"Las contraseñas no coinciden"}, status=status.HTTP_400_BAD_REQUEST)

            instance = query[0]
            instance.set_password(self.request.data['password1'])

            if (instance.last_login == None):
                instance.last_login = timezone.now()
            else:
                instance.last_login = None

            instance.save()

            return Response({"ok":True}, status=status.HTTP_200_OK)

        except:
            return Response({'error':"Error de autenticación. Por favor logearse de nuevo. Si el problema persiste, "
                                "favor comunicarse con el administrador de la plataforma"},status=status.HTTP_400_BAD_REQUEST)

class ResetPassword(APIView):
    def post(self, request):

        print(request.data['email'])
        query = User.objects.filter(email=self.request.data['email'])

        if ( len(query) < 1 ):
            return Response({'error': 'El correo ingresado no se encuentra registrado en la plataforma'}, status=status.HTTP_400_BAD_REQUEST)

        user = query[0]

        if ( user.is_active ):

            #Crear un token aleatorio para el envio del correo de verficación
            token_generator = default_token_generator.make_token(user)
            print(token_generator)

            #===============================Envio de correo con enlace de recuperación==================================
            #create message object instance
            msg = MIMEMultipart()
            uidb64 = urlsafe_base64_encode(force_bytes(user.pk))

            message = "Usted solicito cambio de contraseña, favor ingresar a este enlace " \
                      "http://localhost:4200/#/responseResetPassword/" + uidb64 + "[0-9A-Za-z_%5C-]+/" + token_generator

            # setup the parameters of the message
            password = "Sabi@c@l@mbia"
            msg['From'] = "contacto.datosabios@gmail.com"
            msg['To'] = query[0].email
            msg['Subject'] = "Cambio de contraseña"

            # add in the message body
            msg.attach(MIMEText(message, 'plain'))

            # create server
            server = smtplib.SMTP('smtp.gmail.com: 587')

            server.starttls()

            # Login Credentials for sending the mail
            server.login(msg['From'], password)

            # send the message via the server.
            server.sendmail(msg['From'], msg['To'], msg.as_string())
            server.quit()

            print("Correo enviado con exito a %s" % (msg['To']))

            return Response({"ok": True}, status=status.HTTP_200_OK)

        else:
            print("El usuario se encuentra inactivo")
            return Response({'message': 'El usuario se encuentra inactivo'}, status=status.HTTP_200_OK)

UserModel = get_user_model()

class ResponseResetPassword(APIView):
    token_generator = default_token_generator
    reset_url_token = 'set-password'

    def get(self, request, *args, **kwargs):
        assert 'uidb64' in kwargs and 'token' in kwargs

        self.validlink = False
        self.user = self.get_user(kwargs['uidb64'])

        print(type(self.user.username))

        if self.user is not None:
            token = kwargs['token']
            print(token)

            if self.token_generator.check_token(self.user, token):
                # If the token is valid, display the password reset form.
                print("Token valido")
                self.validlink = True
                return Response({"ok":"Token válido", "username": self.user.username, "pk":self.user.pk}, status=status.HTTP_200_OK)

        # Display the "Password reset unsuccessful" page.
        return Response({"error":"Problemas con la autenticación del usuario"}, status=status.HTTP_400_BAD_REQUEST)

    def get_user(self, uidb64):
        try:
            # urlsafe_base64_decode() decodes to bytestring
            uid = urlsafe_base64_decode(uidb64).decode()
            user = UserModel._default_manager.get(pk=uid)
        except (TypeError, ValueError, OverflowError, UserModel.DoesNotExist, ValidationError):
            user = None
        return user

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['user'] = self.user
        return kwargs

