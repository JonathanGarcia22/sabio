import os, logging

class saveLogDescargas:
    def __init__(self):
        self.levelname = 'info'

    def guardarDescarga(self, mensaje):
        fichero_logs = os.path.join(os.getcwd(), "Descargas", "logsDescargas")

        print(fichero_logs)

        print('Archivo Log en ', fichero_logs)
        logging.basicConfig(level=logging.INFO,
                            format='%(asctime)s : %(levelname)s : %(message)s',
                            filename = fichero_logs,
                            filemode = 'a',)
        logging.info(mensaje)

        logging.shutdown()