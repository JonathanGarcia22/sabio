from rest_framework.response import Response
from .serializers import UserSerializer
from rest_framework.views import APIView
from rest_framework import status, generics
from .models import User

from pymongo import MongoClient
from bson.json_util import dumps
import json

from ..logs import saveLog


class UserAPI_Create(APIView):
    def post(self,request):
        try:
            print(request.data)
            idResponsable = request.data['id']
            usuarioResponsable = User.objects.get(id=idResponsable).username

            serializer = UserSerializer(data=request.data['usuario'])      #En DRF los datos estan en request.data y no en request.post
                                                                # como cuando se programa backend

            if ( serializer.is_valid() ):
                email = serializer.validated_data['email']
                usuario = User.objects.filter(email=email)

                #Valida si el email es unico
                if len(usuario) != 0:
                    return Response({"error":{'username':["Este correo de usuario ya existe, ingrese uno nuevo"]}}, status=status.HTTP_400_BAD_REQUEST)

                user = serializer.save()

                log = saveLog("info")
                log.guardar("{'accion':'crear usuario', 'estado':'terminado', 'responsable':'{'id':" + idResponsable +
                            ", 'username':" + usuarioResponsable + "}', 'respuesta': " + str(serializer.data) + '}')

                return Response(serializer.data, status=status.HTTP_201_CREATED)
            else:
                log = saveLog("warning")
                log.guardar("{'accion':'crear usuario', 'estado':'fallido', 'responsable':'{'id':" + idResponsable +
                            ", 'username':" + usuarioResponsable + "}', 'respuesta': " + str(serializer.errors) + '}')
                return Response({'error':serializer.errors},
                                status=status.HTTP_400_BAD_REQUEST)
        except:
            return Response({'error':{'not_register':['El usuario que intento ejecutar esta acción no se encuentra registrado']}},
                            status=status.HTTP_400_BAD_REQUEST)

class updateUser(APIView):
    def put(self,request):
        try:
            print(request.data)
            idResponsable = request.data['id']
            usuarioResponsable = User.objects.get(id=idResponsable).username

            user = User.objects.get(id=request.data['usuario']['id'])

            print( (request.data['usuario']['is_active']))
            instance = user
            instance.username = request.data['usuario']['username']
            instance.first_name = request.data['usuario']['first_name']
            instance.email = request.data['usuario']['email']
            instance.rol = request.data['usuario']['rol']
            if ((request.data['usuario']['rol'] == 'ADMIN') or (request.data['usuario']['rol'] == 'SANOFI') or (request.data['usuario']['rol'] == 'VISITANTE')):
                instance.entidad = 'TODAS'
            elif ( request.data['usuario']['rol'] == 'HMO' ):
                instance.entidad = request.data['usuario']['entidad'].upper()
            instance.is_active = request.data['usuario']['is_active']
            instance.save()

            log = saveLog("info")
            log.guardar("{'accion':'actualizar usuario', 'estado':'terminado', 'responsable':'{'id':" + idResponsable +
                        ", 'username':" + str(usuarioResponsable) + "}', {usuario actualizado: " + str(user) + "} 'respuesta': usuario actualizado con exito}")

            return Response({'ok': "Usuario " + user.username + " actualizado con éxito"}, status=status.HTTP_200_OK)
        except:
            log = saveLog("warning")
            log.guardar("{'accion':'actualizar usuario', 'estado':'fallido', 'responsable':'{'id':" + idResponsable +
                        ", 'username':indefinido}', 'respuesta': Usuario no encontrado, no se puedo actualizar}")

            return Response({'error':'Usuario no encontrado, no se puedo actualizar'}, status=status.HTTP_400_BAD_REQUEST)

class ListUsers(generics.ListCreateAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer

    def list(self, request):
        queryset = self.get_queryset()
        serializer = UserSerializer(queryset, many=True)
        return Response(serializer.data)

class rolForId (APIView):
    def post(self, request):
        try:
            id = request.data['id']
            usuario = User.objects.get(id=id)
            rol = usuario.rol

            return Response({"rol":rol},status=status.HTTP_200_OK)
        except:
            return Response({"error":"No se pudo validar la identificación del usuario"},status=status.HTTP_400_BAD_REQUEST)