from django.urls import path
from .views import UserAPI_Create, ListUsers, updateUser, rolForId

urlpatterns = [
    path('create_user/', UserAPI_Create.as_view(),name='api_create_user'),
    path('list_user/', ListUsers.as_view(),name='api_list_user'),
    path('update_user/', updateUser.as_view(),name='api_update_user'),
    path('rol_for_id/', rolForId.as_view(),name='rol_for_id')
]