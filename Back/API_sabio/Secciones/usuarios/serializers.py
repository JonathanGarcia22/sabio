from rest_framework import serializers
from .models import User
from django.utils import timezone

class UserSerializer(serializers.Serializer):
    id = serializers.ReadOnlyField()            #Campo de solo lectura
    first_name = serializers.CharField(required=False, default="")
    last_name = serializers.CharField(required=False, default="")
    is_superuser = serializers.CharField(required=False, default=False)
    last_login = serializers.DateTimeField(required=False, default=None)
    date_joined = serializers.DateTimeField(required=False, default=timezone.now)
    username = serializers.CharField()
    email = serializers.EmailField()
    password = serializers.CharField()
    rol = serializers.CharField()
    entidad = serializers.CharField(required=False, default='todas')
    is_active = serializers.BooleanField(default=True)
    counter = serializers.IntegerField(required=False, default=0)
    date_counter = serializers.DateTimeField(required=False, default=timezone.now)

                                                # validated_data es un diccionario que contiene la info que ha sido
    def create(self, validated_data):           # pasada a traves de la petición y va a contener los campos definidos arriba
        instance = User()
        instance.first_name = validated_data.get('first_name')
        instance.last_name = validated_data.get('last_name')
        instance.is_superuser = validated_data.get('is_superuser')
        instance.last_login = validated_data.get('last_login')
        instance.date_joined = validated_data.get('date_joined')
        instance.username = validated_data.get('username')
        instance.email = validated_data.get('email')
        instance.set_password(validated_data.get('password'))
        instance.rol = validated_data.get('rol').upper()
        instance.entidad = validated_data.get('entidad').upper()
        instance.is_active = validated_data.get('is_active')
        instance.counter = validated_data.get('counter')
        instance.date_counter = validated_data.get('data_counter')
        instance.save()
        return instance

    #En orden de ejecución esta función se ejecuta primero que def create(self, validated_data)
    def validate_username(self, data):
        usuario = User.objects.filter(username=data)

        if len( usuario ) != 0:
            raise serializers.ValidationError("Este nombre de usuario ya existe, ingrese uno nuevo")
        else:
            return data