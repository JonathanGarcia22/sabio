from django.contrib import admin
from .models import User
from import_export import resources
from import_export.admin import ImportExportModelAdmin

class UserResource(resources.ModelResource):
    class Meta:
        model = User

class UserAdmin(ImportExportModelAdmin,admin.ModelAdmin):
    list_display = ('username','email','rol','last_login','is_active', 'is_superuser', 'is_staff')
    #search_fields = ['nombre','apellidos','email']
    resource_class = UserResource

admin.site.register(User, UserAdmin)
