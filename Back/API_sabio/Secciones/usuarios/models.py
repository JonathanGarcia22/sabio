import uuid

from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils import timezone

class User(AbstractUser):
    Roles = (
        ('admin', 'Adminstrador'),
        ('hmo', 'HOM'),
        ('sanofi', 'SANOFI'),
        ('basico','Básico'),
    )

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    rol = models.CharField("Rol", max_length=20, choices=Roles)
    entidad = models.CharField("Entidad", max_length=100, blank=False, null=False)
    counter = models.IntegerField(blank=True, null=True, default=0)
    date_counter = models.DateTimeField(default=timezone.now)

    class Meta:
        db_table = 'auth_user'

