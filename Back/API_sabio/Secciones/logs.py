import os, logging

class saveLog:
    def __init__(self, nivel):
        self.levelname = nivel

    def guardar(self, mensaje):
        fichero_log = os.path.join(os.getcwd(), 'Secciones', 'logs')

        print(fichero_log)

        print('Archivo Log en ', fichero_log)
        logging.basicConfig(level=logging.DEBUG,
                            format='%(asctime)s : %(levelname)s : %(message)s',
                            filename = fichero_log,
                            filemode = 'a',)

        if (self.levelname == "info" ):
            logging.info(mensaje)
            #logging.info('{"id": "adfsafdsfdsfd", "username":"juan","rol":"admin","peticion":"bring_result","args":"Null","respuesta":"[]"}')
        elif ( self.levelname == "warning" ):
            logging.warning(mensaje)
            #logging.warning('{"id": "adfsafdsfdsfd", "username":"carlos","rol":"hmo","peticion":"list_user","args":"Null","respuesta":"[]"}')
        logging.shutdown()