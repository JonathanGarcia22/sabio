from django.urls import path
from .views import consultaResultado, enfermedades_departamentos, entidadUsuario, entidadesUsuario, \
    enviarResultados, borrarArchivos, actualizarEstado, datosLogsDescargas, listaArchivosBorrar

urlpatterns = [
    path('bring_result/', consultaResultado.as_view(), name='resultados'),
    path('list_entities_par_user/',entidadesUsuario.as_view(),name='listar_entidades_por_usuario'),
    path('list_diseases_places/', enfermedades_departamentos.as_view(), name='listar_enfermedades'),
    path('user_entity_permission/', entidadUsuario.as_view(), name='permiso_usuario_entidad'),
    path('send_result/', enviarResultados.as_view(), name='send-result'),
    path('delete_txt/', borrarArchivos.as_view(), name='delete_txt'),
    path('update_state/', actualizarEstado.as_view(), name='update_state'),
    path('data_logs_downloads/', datosLogsDescargas.as_view(), name='data_logs_downloads'),
    path('name_to_delete/', listaArchivosBorrar.as_view(), name='name_to_delete')
]