import smtplib
from email import encoders
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from uuid import UUID
import os
from datetime import datetime

from bson import ObjectId
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from pymongo import MongoClient, ReturnDocument
from bson.json_util import dumps
import json
import numpy as np
import pandas as pd
import operator

from rest_framework import generics
from ..logs import saveLog


def entidades():
    client = MongoClient("mongodb://EnfrarasBIOS:ProyBIOS2019!@190.7.134.182:27018")
    db = client.resultados
    collections = db.list_collection_names()

    list_entities = list()

    for collection in collections:
        if ('-' in collection):
            entidad = collection.split("-")[1]
            if (not entidad.upper() in list_entities):
                list_entities.append(entidad.upper())

    return list_entities

#Regresa la lista de entidades a las cuales puede acceder un usuario
class entidadesUsuario(APIView):
    def post(self, request):
        id = self.request.data['id']
        list_entities = list()

        #client = MongoClient("localhost", 27017, authSource='SabioDB_Dashboard')
        client = MongoClient("mongodb://EnfrarasBIOS:ProyBIOS2019!@190.7.134.182:27018")
        db = client.usuarios
        usuario = dict(db.auth_user.find_one({'id': UUID(id)}))
        rol = usuario['rol']

        entitie = usuario['entidad']

        if ( entitie == "TODAS" ):
            list_entities = entidades()
        else:
            list_entities = entitie

        return Response({'entidades': list_entities}, status=status.HTTP_200_OK)

#Verifica que el usuario que esta logeado de verdad puede acceder a la entidad la cual quiere consultar
class entidadUsuario(APIView):
    def post(self, request):
        try:
            id = self.request.data['id']
            entidad_front = self.request.data['entidad'].upper()
            permiso = False

            #client = MongoClient("localhost", 27017, authSource='SabioDB_Dashboard')
            client = MongoClient("mongodb://EnfrarasBIOS:ProyBIOS2019!@190.7.134.182:27018")
            db = client.usuarios
            usuario = dict(db.auth_user.find_one( { 'id': UUID(id)} ))
            print(usuario)
            entidades_back = usuario['entidad']
            print(entidades_back)

            if ( entidades_back == "TODAS" ):
                entidades_back = entidades()

            print(entidad_front)

            if ( entidad_front in entidades_back ):
                permiso = True

            log = saveLog("info")
            log.guardar("{'accion':'verficar que este usuario puede acceder a la entidad '" + str(entidad_front) + ", 'estado':'terminado', 'responsable':'{'id':" + id +
                        ", 'username':" + usuario["username"] + "}', 'respuesta': " + str(permiso) + '}')

            return Response({'permiso':permiso})
        except:
            permiso = False
            log = saveLog("warning")
            log.guardar("{'accion':'verficar que este usuario puede acceder a la entidad '" + str(
                entidad_front) + ", 'estado':'fallido', 'responsable':'{'id':" + id +
                        ", 'username': usuario NO encontrado (es posible que el id no fuera válido)}', 'respuesta': " + str(permiso) + '}')
            return Response({'permiso': permiso})

class enfermedades_departamentos(APIView):
    def post(self, request):
        client = MongoClient("mongodb://EnfrarasBIOS:ProyBIOS2019!@190.7.134.182:27018")
        db = client.resultados
        collections = db.list_collection_names()
        print("Colecciones totales")
        print(collections)

        entidad = (self.request.data['entidad']).upper()

        if ((len(entidad) > 0) and (entidad != " ")):
            list_diseases = list()

            resultado = dict()

            for collection in collections:
                if (entidad in collection):
                    departamentos = list()
                    enfermedad = collection.split("-")
                    if ( len(enfermedad) == 1 ):
                        pass
                    else:
                        enfermedad = enfermedad[0]

                        ubicacion = db[collection].find({'$and': [{
                                                                'score': {'$gt': 0},
                                                                'ubicacion': {'$exists': True},
                                                                'ubicacion.codigodepartamento': {'$ne': np.nan},
                                                                'ubicacion.codigomunicipio': {'$ne': np.nan}
                                                            }]
                                                        }, {"ubicacion": 1, "_id": 0})

                        dump = dumps(ubicacion)
                        ubicacion = json.loads(dump)

                        for objecto in ubicacion:
                            valor = int(float(list(objecto['ubicacion'].values())[0]))
                            if (not valor in departamentos):
                                departamentos.append(valor)

                        resultado[enfermedad] = departamentos

            if ( len(resultado.keys()) == 0 ):
                return Response({'info': 'No se encontraron resultados para la entidad seleccionada'}, status=status.HTTP_204_NO_CONTENT)
            else:
                return Response(resultado, status=status.HTTP_200_OK)
        else:
            return Response({'error': 'Debe seleccionar una entidad'}, status=status.HTTP_204_NO_CONTENT)

def enviarCorreo(destinatario, rutaArchivo, nombreArchivo):
    # Iniciamos los parámetros del script
    remitente = "contacto.datosabios@gmail.com"
    password = "Sabi@c@l@mbia"
    destinatarios = destinatario
    asunto = 'Archivo solicitado a la plataforma SABIO'
    cuerpo = 'Archivo de texto con registros solicitados en la plataforma SABIO'
    ruta_adjunto = rutaArchivo
    nombre_adjunto = nombreArchivo

    # Creamos el objeto mensaje
    mensaje = MIMEMultipart()

    # Establecemos los atributos del mensaje
    mensaje['From'] = remitente
    mensaje['To'] = destinatarios
    mensaje['Subject'] = asunto

    # Agregamos el cuerpo del mensaje como objeto MIME de tipo texto
    mensaje.attach(MIMEText(cuerpo, 'plain'))

    # Abrimos el archivo que vamos a adjuntar
    archivo_adjunto = open(ruta_adjunto, 'rb')

    # Creamos un objeto MIME base
    adjunto_MIME = MIMEBase('application', 'octet-stream')
    # Y le cargamos el archivo adjunto
    adjunto_MIME.set_payload((archivo_adjunto).read())
    # Codificamos el objeto en BASE64
    encoders.encode_base64(adjunto_MIME)
    # Agregamos una cabecera al objeto
    adjunto_MIME.add_header('Content-Disposition', "attachment; filename= %s" % nombre_adjunto)
    # Y finalmente lo agregamos al mensaje
    mensaje.attach(adjunto_MIME)


    # Creamos la conexión con el servidor
    sesion_smtp = smtplib.SMTP('smtp.gmail.com', 587)

    # Ciframos la conexión
    sesion_smtp.starttls()

    # Iniciamos sesión en el servidor
    sesion_smtp.login(remitente, password)

    # Convertimos el objeto mensaje a texto
    texto = mensaje.as_string()

    # Enviamos el mensaje
    sesion_smtp.sendmail(remitente, destinatarios, texto)

    # Cerramos la conexión
    sesion_smtp.quit()

    print("Correo enviado")

class enviarResultados(APIView):
    def post(self, request):
        try:
            id = self.request.data['id']

            try:
                #client1 = MongoClient("localhost", 27017, authSource='SabioDB_Dashboard')
                client1 = MongoClient("mongodb://EnfrarasBIOS:ProyBIOS2019!@190.7.134.182:27018")
                db = client1.usuarios
                usuario = dict(db.auth_user.find_one({'id': UUID(id)}))
                email = usuario['email']

                print(usuario)
                print(email)

                if ( usuario['rol'] == 'VISITANTE' ):
                    log = saveLog("warning")
                    log.guardar("{'accion':'enviar correo con registros anonimizados ', "
                                "'estado':'fallido', 'responsable':'{'id':" + id +
                                ", 'username':" + usuario["username"] + "}', 'respuesta':No tiene permisos para descargar esta información (usuario VISITANTE)}")

                    return Response({'error': "No tiene permisos para descargar esta información"}, status=status.HTTP_400_BAD_REQUEST)
            except:
                log = saveLog("warning")
                log.guardar("{'accion':'enviar correo con registros anonimizados ', "
                            "'estado':'fallido', 'responsable':'{'id':" + id +
                            ", 'username': usuario NO encontrado}', 'respuesta':No tiene permisos para descargar esta información (usuario no encontrado)}")

                return Response({'error':"No tiene permisos para descargar esta información"}, status=status.HTTP_400_BAD_REQUEST)

            collection = self.request.data['collection']
            ids = self.request.data['ids']

            print(collection)

            client2 = MongoClient("mongodb://EnfrarasBIOS:ProyBIOS2019!@190.7.134.182:27018")
            db = client2.resultados

            print("Descargando ", len(ids), " resultados")

            encabezado = '"id"║"id_cifrado"║"edad"║"genero"║"regimen"║"score"║"signos"║"estado"'
            estampa_tiempo = datetime.timestamp(datetime.now())
            nombreArchivo = usuario['username'] + '_' + collection + '_' + str(estampa_tiempo) + '.txt'

            ruta = os.path.join(os.getcwd(), ('Descargas' + '\\' + nombreArchivo))

            with open(ruta, "w", encoding="utf-8") as f:
                f.write(encabezado+'\n')

                for i in range(len(ids)):
                    #result.append(json.loads(dumps(db[collection].find({'_id':ObjectId(resultados[i])}))))
                    result = json.loads(dumps(db[collection].find({'_id': ObjectId(ids[i])})))

                    id = dict(result[0])['_id']['$oid']
                    id_cifrado = str(dict(result[0])['id_cifrado'])
                    if ( id_cifrado.__contains__('"') ):
                        id_cifrado = id_cifrado.replace('"','""')
                    edad = dict(result[0])['edad']
                    genero = dict(result[0])['genero']
                    regimen = dict(result[0])['regimen']
                    score = dict(result[0])['score']
                    signos = list(dict(dict(result[0])['signos']).values())[0]
                    estado = result[0]['estado']

                    doc = '\"'+id+'\"'+'║'+'\"'+id_cifrado+'\"'+'║'+'\"'+str(edad)+'\"'+'║'+'\"'+genero+'\"'\
                        +'║'+'\"'+regimen+'\"'+'║'+'\"'+str(score)+'\"'+'║'+'\"'+str(signos)+'\"'+'║'+'\"'+str(estado)+'\"'
                    f.write(doc+'\n')

            f.close()

            #log_descargas = saveLogDescargas()
            #log_descargas.guardarDescarga("{'usuarios':" + usuario['username'] + ", 'collection':" + collection + "}")
            rutaDescargas = os.path.join(os.getcwd(), "Descargas", "logsDescargas")
            with open(rutaDescargas, "a", encoding="utf-8") as f:
                f.write(str(datetime.now()) + " : INFO : " + str({'usuario':usuario['username'], 'collection': collection}) +'\n')
            f.close()

            print(nombreArchivo, " guardado")

            enviarCorreo(email, ruta, nombreArchivo)

            return Response('La información ha sido enviada al correo regitrado', status=status.HTTP_200_OK);
        except:
            log = saveLog("warning")
            log.guardar("{'accion':'enviar correo con registros anonimizados ', "
                        "'estado':'fallido', 'responsable':'{'id':" + id +
                        ", 'username':" + usuario[
                            "username"] + "}', 'respuesta':correo no enviado es posible que hubierán problemas con el servidor de correo, "
                                          "o con el guardado de la info en el archivo de logs de descargas}")

            return Response({'error':"No fue posible descargar el archivo. Comuniquese con el adminitrador"}, status=status.HTTP_400_BAD_REQUEST)

class datosLogsDescargas(APIView):
    def get(self,request):
        fileDownloads = os.path.join(os.getcwd(), 'Descargas', 'logsDescargas')

        data = list()
        listEnfermedad = list()
        with open(fileDownloads, "r") as f:
            for linea in f:
                if ( (not linea.__contains__("\\")) and (linea.__contains__("INFO")) and (linea.__contains__("usuario")) ):
                    nombre = ( ( ( ((linea.split(" : "))[-1]).split(",")[0] ).split(":")[-1] ) ).replace("'", "")
                    enfermedad = ( ( ( ( ((linea.split(" : "))[-1]).split(",")[1] ).split(":")[-1] ).split("-") )[0] ).replace("'", "")
                    entidad = ( ( ( ( ( ((linea.split(" : "))[-1]).split(",")[1] ).split(":")[-1] ).split("-") )[-1]).split("}")[0] ).replace("'", "")
                    fecha = (linea.split(" : ")[0])

                    listEnfermedad.append(enfermedad.upper())
                    data.append({'nombre': nombre, 'enfermedad':enfermedad, 'entidad':entidad, 'fecha':fecha})

        lista_enfermedades, cantidad_enfermedades = np.unique(listEnfermedad, return_counts=True)
        dataEnfermedades = list()

        for i in range(len(lista_enfermedades)):
            dataEnfermedades.append({'value':cantidad_enfermedades[i], 'name':lista_enfermedades[i]})

        return Response({'logsDescargas':data, 'labelEnfermedad':lista_enfermedades, 'dataEnfermedad':dataEnfermedades}, status=status.HTTP_200_OK)

class listaArchivosBorrar(APIView):
    def get(self, request):
        nombre = list()
        rutaArchivos = os.path.join(os.getcwd(), 'Descargas')
        archivos = os.listdir(rutaArchivos)
        print(archivos)
        for file in archivos:
            if (file != 'logsDescargas'):
                nombre.append(file)

        return Response({'nombres':nombre}, status=status.HTTP_200_OK)

class borrarArchivos(APIView):
    def post(self, request):
        id = self.request.data['id']

        try:
            #client1 = MongoClient("localhost", 27017, authSource='SabioDB_Dashboard')
            client1 = MongoClient("mongodb://EnfrarasBIOS:ProyBIOS2019!@190.7.134.182:27018")
            db = client1.usuarios
            usuario = dict(db.auth_user.find_one({'id': UUID(id)}))
            rol = usuario['rol']

            print(usuario)
            print(rol)

            if (usuario['rol'] != 'ADMIN'):
                log = saveLog("warning")
                log.guardar("{'accion':'borrar nuevos archivos que han sido enviados por correo ', "
                            "'estado':'fallido', 'responsable':'{'id':" + id +
                            ", 'username':" + usuario[
                                "username"] + "}', 'respuesta':No tiene permisos para descargar esta información (usuario diferente a ADMIN)}")

                return Response({'error': "No tiene permisos para ejecutar esta acción"},
                                status=status.HTTP_400_BAD_REQUEST)

            else:
                rutaArchivos = os.path.join(os.getcwd(), 'Descargas')
                archivos = os.listdir(rutaArchivos)
                print(archivos)
                for file in archivos:
                    if ( file != 'logsDescargas'):
                        os.remove(rutaArchivos + '\\' + file)

                log = saveLog("info")
                log.guardar("{'accion':'borrar nuevos archivos que han sido enviados por correo ', "
                            "'estado':'terminado', 'responsable':'{'id':" + id +
                            "'username':" + usuario[
                                "username"] + "}', respuesta: Archivos borrados con éxito}', '")

                return Response({'Archivos borrados con éxito'}, status=status.HTTP_200_OK)
        except:
            log = saveLog("warning")
            log.guardar("{'accion':'borrar nuevos archivos que han sido enviados por correo ', "
                        "'estado':'fallido', 'responsable':'{'id':" + id +
                        ", 'username':indefinido}', respuesta: es posible que no se encontrara el usuario con el id enviado desde el front}")

            return Response({'error': "No fue posible borrar los archivos"},
                           status=status.HTTP_400_BAD_REQUEST)


class actualizarEstado(APIView):
    def post(self, request):
        try:
            client = MongoClient("mongodb://EnfrarasBIOS:ProyBIOS2019!@190.7.134.182:27018")
            db = client.resultados

            #client1 = MongoClient("localhost", 27017, authSource='SabioDB_Dashboard')
            client1 = MongoClient("mongodb://EnfrarasBIOS:ProyBIOS2019!@190.7.134.182:27018")
            db1 = client1.usuarios

            idResponsable = self.request.data['idresponsable']
            usuarioResponsable = dict(db1.auth_user.find_one({'id': UUID(idResponsable)}))
            entidad = usuarioResponsable['entidad']

            idRegistro = self.request.data['idpaciente']
            nuevoEstado = self.request.data['estado'].lower()
            collection = self.request.data['collection']

            print(usuarioResponsable)

            if ( entidad.lower() != (collection.split("-")[1]).lower() ):
                log = saveLog("warning")
                log.guardar("{'accion':'actualizar estado de tamizaje de una persona ', "
                            "'estado':'fallido', 'responsable':'{'id':" + idResponsable +
                            "'username':" + usuarioResponsable[
                                "username"] + "}', respuesta:el usuario que intento esta accion no permenece a esta entiad }', '")

                return Response({'error': "No tiene permisos para ejecutar esta acción"},
                                status=status.HTTP_400_BAD_REQUEST)

            listaTotal = ['nulo', 'en proceso', 'interrumpido', 'positivo', 'negativo']

            if (not nuevoEstado in listaTotal):
                log = saveLog("warning")
                log.guardar("{'accion':'actualizar estado de tamizaje de una persona ', "
                            "'estado':'fallido', 'responsable':'{'id':" + idResponsable +
                            "'username':" + usuarioResponsable[
                                "username"] + "}', respuesta: El estado indicado no es válido }', '")

                return Response({'error': 'El estado indicado no es válido'}, status=status.HTTP_400_BAD_REQUEST)
            else:
                update = db[collection].find_one_and_update( {'_id': ObjectId(idRegistro)},
                                                             {"$set": {"estado." + nuevoEstado: datetime.utcnow()} },
                                                             return_document=ReturnDocument.AFTER)

                estados = sorted(update['estado'].items(), key=operator.itemgetter(1), reverse=True)
                update = json.loads(dumps(update))
                print(update)
                print(estados)

                log = saveLog("info")
                log.guardar("{'accion':'actualizar estado de tamizaje de una persona ', "
                            "'estado':'terminado', 'responsable':'{'id':" + idResponsable +
                            "'username':" + usuarioResponsable[
                                "username"] + "}', respuesta:{ 'registro_actualizado':" +  str(update) + " }  }', '")

                return Response({'registro_actualizado':update, 'estados_ordenados':estados}, status=status.HTTP_200_OK)
        except:
            log = saveLog("warning")
            log.guardar("{'accion':'actualizar estado de tamizaje de una persona ', "
                        "'estado':'fallido', 'responsable':'{'id':" + idResponsable +
                        "'username': indefinido}', respuesta: Es posible que no se encontrara el usuario con el id "
                        "especificado, que hubieran problema con el nombre de la coleccion buscada o que el id de "
                        "la persona a actualizar no fuera valido }', '")

            return Response({'error':'No fue posible actualizar el estado'}, status=status.HTTP_400_BAD_REQUEST)


class consultaResultado(generics.GenericAPIView):

    def post( self, request ):
        try:
            client = MongoClient("mongodb://EnfrarasBIOS:ProyBIOS2019!@190.7.134.182:27018")
            db = client.resultados
            # =============================================Resultados===================================================
            collection = self.request.data['collection']
            print(collection.upper())
            codigoDepartamento = self.request.data['ubicacion']
            cantidad = self.request.data['cantidad']
            print("Cantidad mandada desde front ", cantidad)

            if ( codigoDepartamento == 'TODOS' ):
                print("Imprimiendo resultados para TODOS los departamentos")

                cantidad_total = db[collection].find({'score': {'$gt': 0}}).count()
                # Esto ocurre en el primer cargue de la pagina donde mando el porcentaje de datos que debe cargar
                if ((cantidad > 0) and (cantidad < 1)):
                    cantidad = int(cantidad_total * cantidad)

                data = db[collection].find({ 'score': {'$gt': 0}}).sort([('score', -1)]).limit(cantidad)

                dump = dumps(data)
                json_string = json.loads(dump)

                for i in range(len(json_string)):
                    if (dict(json_string[i]).keys().__contains__("ubicacion")):
                        if (str(json_string[i]['ubicacion']['codigodepartamento']) == str('nan') ):
                            json_string[i]['ubicacion']['codigodepartamento'] = 0
                        if (str(json_string[i]['ubicacion']['codigomunicipio']) == str('nan')):
                            json_string[i]['ubicacion']['codigomunicipio'] = 0

            else:
                print("Imprimiendo resultados para el departamento con codigo ", codigoDepartamento)

                #Posibles codigos
                floatCodigo = float(codigoDepartamento)
                strIntCodigo = str(codigoDepartamento)
                strfloatCodigo = str(float(codigoDepartamento))
                str1zeros = str(0) + str(codigoDepartamento)
                str2zeros = str(0) + str(0) + str(codigoDepartamento)

                ubicaciones = [codigoDepartamento, floatCodigo, strIntCodigo, strfloatCodigo, str1zeros, str2zeros]
                print(ubicaciones)

                cantidad_total = db[collection].find({'$and': [{
                                                    'score': {'$gt': 0},
                                                    'ubicacion': {'$exists': True},
                                                    'ubicacion.codigodepartamento':{'$in':list(ubicaciones)}
                                                }]
                                                }).count()

                # Esto ocurre en el primer cargue de la pagina donde mando el porcentaje de datos que debe cargar
                if ((cantidad > 0) and (cantidad < 1)):
                    cantidad = int(cantidad_total * cantidad)

                data = db[collection].find({'$and': [{
                                                    'score': {'$gt': 0},
                                                    'ubicacion': {'$exists': True},
                                                    'ubicacion.codigodepartamento':{'$in':list(ubicaciones)}
                                                }]
                                                }).limit(cantidad).sort( [('score',-1)] )

                dump = dumps(data)
                json_string = json.loads(dump)

            print("Cantidad de resultados devueltos: ", len(json_string))

            resultado = np.array(json_string)
            edades = np.empty_like(resultado)
            genero = np.empty_like(resultado)
            regimen = np.empty_like(resultado)
            signos = np.empty_like(resultado)

            for i in range( len(resultado) ):
                edades[i] = resultado[i]['edad']
                genero[i] = np.str_(resultado[i]['genero'])
                regimen[i] = np.str_(resultado[i]['regimen'])
                signos[i] = list(resultado[i]['signos'].values())

            # ==============================================Edades======================================================
            rangos = ["0-16","17-25","26-56","57-90"]
            resultado_edades = dict()

            for r in rangos:
                lim_inf = r.split("-")[0]
                lim_sup = r.split("-")[1]

                rango = np.logical_and(edades >= int(lim_inf), edades <= int(lim_sup))
                lista_rango, cantidad_rango = np.unique(rango, return_counts=True)

                if ( len(cantidad_rango) < 2 ):
                    if ( lista_rango[0] == False ):
                        lista_rango = np.array([False, True])
                        cantidad_rango = np.array([cantidad_rango[0],0])
                    elif( lista_rango[0] == True ):
                        lista_rango = np.array([False, True])
                        cantidad_rango = np.array([0,cantidad_rango[0]])

                resultado_edades[r] = {"cantidad":cantidad_rango[1], "porcentaje": round(cantidad_rango[1]*100/len(edades), 2)}

            # ==============================================Género======================================================
            lista_genero, cantidad_genero = np.unique(genero, return_counts=True)
            resultado_genero = dict()
            print(lista_genero)
            print(cantidad_genero)
            for gen in lista_genero:
                if ( (gen.lower() != "femenino") and (gen.lower() != "masculino") ):
                    indice = np.argwhere(lista_genero == gen)
                    lista_genero = np.delete(lista_genero, indice)
                    cantidad_genero = np.delete(cantidad_genero, indice)
            print(lista_genero)

            lista_genero = np.array([x.capitalize() if isinstance(x, str) else x for x in lista_genero])

            for i in range(2):
                if (len(lista_genero) < 2):
                    if (lista_genero[0] == "Femenino"):
                        lista_genero = np.array(["Femenino", "Masculino"])
                        cantidad_genero = np.array([cantidad_genero[0], 0])
                    elif (lista_genero[0] == "Masculino"):
                        lista_genero = np.array(["Femenino", "Masculino"])
                        cantidad_genero = np.array([0, cantidad_genero[0]])
                if (i == 0):
                    resultado_genero[lista_genero[i]] = {"cantidad": cantidad_genero[i], "porcentaje": round( (cantidad_genero[i] * 10000 / len(genero))/100) }
                elif (i!=0):
                    resultado_genero[lista_genero[i]] = {"cantidad": cantidad_genero[i], "porcentaje": round (100. - (cantidad_genero[i-1] * 10000 / len(genero))/100)}


            # ==============================================Signos======================================================
            unpack_list = list()

            for lista in signos:
                for item in lista:
                    unpack_list.append(item)

            signos = np.array(unpack_list)
            lista_signos, cantidad_signos = np.unique(signos, return_counts=True)

            resultado_signos = dict(zip(lista_signos, cantidad_signos))
            resultado_signos = dict( sorted(resultado_signos.items(), key=operator.itemgetter(1), reverse=True) )

            # ==============================================Regimen=====================================================
            lista_regimen, cantidad_regimen = np.unique(regimen, return_counts=True)

            resultado_regimen = dict( zip(lista_regimen, cantidad_regimen) )
            resultado_regimen = dict( sorted(resultado_regimen.items(), key=operator.itemgetter(1), reverse=True) )

            print("Finalizo carga de resultados")

            return Response({"edades":resultado_edades, "genero":resultado_genero, "signos":resultado_signos,
                            "regimen": resultado_regimen, "cantidad_resultados":cantidad_total,
                             "cantidad_devueltos":len(json_string),"resultados":json_string}, status=status.HTTP_200_OK)

        except:
            return Response({'error':"Ocurrio un error inesperado en la cosulta de la información, favor recargar los "
                                     "datos del filtro. Si el problema persiste, favor comunicarse con el administrador"},
                             status=status.HTTP_400_BAD_REQUEST)

