
export class Usuario {

    constructor(
        public username: string,
        public first_name: string,
        public email: string,
        public password: string,
        public rol?: string,
        public entidad?: string,
        public first_session?: boolean,
        public is_active?: boolean,
        public id?: string,
    ) { }

}
