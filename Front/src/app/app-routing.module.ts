// module Angular
import { NgModule, Component } from '@angular/core';

// Router Module Angular
import { Routes, RouterModule } from '@angular/router';

// Components
import { LandingComponent } from './components/landing/landing.component';
import { AboutComponent } from './components/landing/about.component';
import { LoginComponent } from './components/landing/login.component';
import { ChangepasswordComponent } from './components/landing/changepassword.component';
import { ForgotpasswordComponent } from './components/landing/forgotpassword.component';
import { HmofilterComponent } from './components/landing/hmofilter.component';

// Guards
import { LoginRequiredGuard } from './services/guards/login-required.guard';
import { ProtectedLoginGuard } from './services/guards/protected-login.guard';
import { ResponseResetPasswordComponent } from './components/landing/response-reset-password.component';

const routes: Routes = [
  {path: '', component: LandingComponent, canActivate: [ ProtectedLoginGuard ]},
  {path: 'landing', component: LandingComponent, canActivate: [ ProtectedLoginGuard ]},
  {path: 'about', component: AboutComponent},
  {path: 'login', component: LoginComponent, canActivate: [ ProtectedLoginGuard ]},
  {path: 'changePassword', component: ChangepasswordComponent, canActivate: [ LoginRequiredGuard ]},
  {path: 'responseResetPassword/:uidb64/:token', component: ResponseResetPasswordComponent},
  {path: 'forgotpassword', component: ForgotpasswordComponent},
  {path: 'hmoFilter', component: HmofilterComponent, canActivate: [ LoginRequiredGuard ]},
  /*{path: '**', component: NopagefoundComponent}*/
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
