import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'jsonValues'
})
export class JsonValuesPipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    return (Object.values(value));
  }

}
