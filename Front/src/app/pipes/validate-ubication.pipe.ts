import { Pipe, PipeTransform } from '@angular/core';
import { codigoLugares } from '../diccionarioLugares/dicDepartamentos';

@Pipe({
  name: 'validateUbication'
})
export class ValidateUbicationPipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    if ( typeof(value) == 'undefined' ){
      return 'DESCONOCIDO';
    }
    else{
      let codMunicipio = String(value.codigomunicipio);
      let codDepartamento = String(value.codigodepartamento);

      if ( codMunicipio.length == 1 ){
        codMunicipio = `${0}${0}${codMunicipio}`
      }
      if ( codMunicipio.length == 2 ){
        codMunicipio = `${0}${codMunicipio}`
      }

      return (codigoLugares[`${codDepartamento}${codMunicipio}`])
    }
  }

}
