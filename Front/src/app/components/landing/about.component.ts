import { Component, OnInit } from '@angular/core';
import { NgwWowService } from 'ngx-wow';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styles: []
})
export class AboutComponent implements OnInit {

  constructor(private wowService: NgwWowService) {}

  ngOnInit() {
    this.wowService.init();
  }

  reset() {
    this.wowService.init();
  }

}
