import { Component, OnInit } from '@angular/core';
import { NgwWowService } from 'ngx-wow';
import { NgForm } from '@angular/forms';
import { UsuarioService } from 'src/app/services/usuario/usuario.service';
import { Usuario } from 'src/app/models/usuario.model';
import { Router } from '@angular/router';


@Component({
  selector: 'app-changepassword',
  templateUrl: './changepassword.component.html',
  styles: []
})
export class ChangepasswordComponent implements OnInit {

  username: string;

  constructor(private wowService: NgwWowService,
            public router: Router,
            public _usuarioService: UsuarioService) {}

  ngOnInit() {
    this.wowService.init();
    
    this.username = sessionStorage.getItem('username') || '';
  }

  reset() {
    this.wowService.init();
  }

  cambiarContrasena( form: NgForm ){
    if ( form.invalid ) {
      return;
    }

    this._usuarioService.validaToken(sessionStorage.getItem('token'))
      .subscribe( resp => {

        let usuario: Usuario = JSON.parse(sessionStorage.getItem('usuario'));
        let body = {id:usuario.id, username: usuario.username, password1: form.value.password1, password2:form.value.password2}
    
        this._usuarioService.cambiarContrasena(body)
          .subscribe( resp => {

            if ( resp )
              this.router.navigate(['/hmoFilter'])
            else 
              this.router.navigate(['/changePassword'])
          });

      });
  }

}
