import { Component, OnInit } from '@angular/core';
import { UsuarioService } from 'src/app/services/usuario/usuario.service';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-forgotpassword',
  templateUrl: './forgotpassword.component.html',
  styles: []
})
export class ForgotpasswordComponent implements OnInit {
  
  email: string;
  cargando:Boolean = false;

  constructor(public _usuarioService: UsuarioService,
              public router: Router,) { }

  ngOnInit() {
  }

  resetPassword( form: NgForm ){
    if ( form.invalid ) {
      return;
    }

    let email = form.value.email
    
    this.cargando = true;
    this._usuarioService.resetContrasena( email )
                  .subscribe( resp => {
                    
                    this.cargando = false;
                    this.router.navigate(['../landing'])
                    
                  });
  }

}
