import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router,  } from '@angular/router';
import { UsuarioService } from 'src/app/services/usuario/usuario.service'
import { FiltroBoardcardsService } from '../shared/filtro-boardcards.service';
import { BnNgIdleService } from 'bn-ng-idle';
import { Usuario } from 'src/app/models/usuario.model';
import { codigoDepartamentos } from '../../diccionarioLugares/dicDepartamentos';
import Swal from 'sweetalert2';

declare const initMapCol: any;

@Component({
  selector: 'app-hmofilter',
  templateUrl: './hmofilter.component.html',
  styles: []
})
export class HmofilterComponent implements OnInit {

  mySubscription: any;
  entidades:string[];
  enfermedades:string[];
  departamentos:string[];
  departamentosMostrar:string[];

  enfermedad_seleccionada:string;
  entidad_seleccionada:string;
  departamento_seleccionado:string;

  cargando:Boolean = false;

  constructor(public router: Router, public _usuarioService: UsuarioService, private bnIdle: BnNgIdleService,
              public _actualizarFiltros: FiltroBoardcardsService) { 
  }

  ngOnInit() {
    //comunicacion entre components que no son padre - hijo
    //this._usuarioService.customMessage.subscribe( msg => console.log( msg ) );
    
    this._actualizarFiltros.enfermedades = []
    this._actualizarFiltros.entidades = []
    this._actualizarFiltros.departamentos = []
    this._actualizarFiltros.signos = []
    this._actualizarFiltros.regimen = []

    initMapCol();

    this._usuarioService.validaToken(sessionStorage.getItem('token'))
      .subscribe( resp => {

        sessionStorage.removeItem('enfermedad');
        sessionStorage.removeItem('entidad');

        this._usuarioService.seguimientoInactividad();
        
        let usuario: Usuario = JSON.parse(sessionStorage.getItem('usuario'));
        let id = usuario.id

        this._usuarioService.listarEntidadesPorUsuario( id )
          .subscribe( resp => {
            let entities:string = String(Object.values(resp));
            this.entidades = entities.split(",");
          });
      });
  }

  onChangeEntidad(newValue) {
    this.cargando = true;
    this._usuarioService.validaToken(sessionStorage.getItem('token'))
      .subscribe( resp => {

        let entitie:String = String(newValue).toLowerCase();
    
        let arr_enfermedades = [];
        
        let arr_departamentos = [];
        let convertirCodigos = [];
    
        this._usuarioService.cargarEnfermedadesDepartamentos( entitie )
          .subscribe(resp => {

            if ( resp == null ){
              Swal.fire({
                icon:'info',
                text: 'No se encontraron resultados para la entidad seleccionada'
              });
            }
            else{
              let enfermedades = Object.keys(resp);
  
              this.enfermedades = enfermedades;
  
              this.departamentos = resp;
              this._actualizarFiltros.respuesta_enfermedades_departamentos = resp;
              sessionStorage.setItem('enfermedadesDepartamentos', JSON.stringify(resp));
              
            }
            this.cargando = false;
          })           
    });
  }  

  onChangeEnfermedad( newValue ){
    let convertirCodigos = [];
    
    let enfermedad = String(newValue).toLowerCase();
    
    this.departamentosMostrar = this.departamentos[enfermedad];
    
    //Convertir los codigos de departamento a sus respectivos nombres
    for (let index = 0; index < this.departamentosMostrar.length; index++) {
      let codigo = this.departamentosMostrar[index];
      if (codigoDepartamentos[ codigo ] != null){
        convertirCodigos.push(codigoDepartamentos[ codigo ]);
      }
    }
    
    this.departamentosMostrar = ['TODOS'].concat(convertirCodigos);
  }

  buscar( form: NgForm ){
    this.enfermedad_seleccionada = form.value.enfermedad;
    this.entidad_seleccionada = form.value.entidad;
    this.departamento_seleccionado = form.value.departamento;
    
    this._usuarioService.validaToken(sessionStorage.getItem('token'))
      .subscribe( resp => {
        
        sessionStorage.setItem("enfermedad", this.enfermedad_seleccionada);
        sessionStorage.setItem("entidad", this.entidad_seleccionada);
        sessionStorage.setItem("departamento", this.departamento_seleccionado);
        sessionStorage.setItem("listaEnfermedades", JSON.stringify(this.enfermedades).toUpperCase());
        sessionStorage.setItem("listaEntidades", JSON.stringify(this.entidades).toUpperCase());
        sessionStorage.setItem("listaDepartamentos", JSON.stringify(this.departamentosMostrar).toUpperCase());

        this.router.navigate(['/dashboard'])
      });
  }

}
