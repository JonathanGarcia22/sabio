import { Component, OnInit } from '@angular/core';
import { NgwWowService } from 'ngx-wow';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { UsuarioService } from 'src/app/services/usuario/usuario.service';

@Component({
  selector: 'app-response-reset-password',
  templateUrl: './response-reset-password.component.html',
  styles: []
})
export class ResponseResetPasswordComponent implements OnInit {

  token:string = "";
  uidb64:string = "";
  username:string = "";
  id:number = 0;

  constructor(private wowService: NgwWowService,
              public _usuarioService: UsuarioService,
              private route:ActivatedRoute,
              public router: Router) {
   }

  ngOnInit() {
    this.route.paramMap.subscribe( (response: ParamMap) => { 

      this.token = response.get('token'),
      this.uidb64 = response.get('uidb64')
    });

    this.wowService.init();

    this._usuarioService.responseResetContrasena( this.uidb64, this.token )
                  .subscribe( resp => {
                    this.username = resp.username
                    this.id = resp.pk

                    this.router.navigate([`/responseResetPassword/${ this.uidb64 }/set-password`])
                      
                  });
  }

  reset() {
    this.wowService.init();
  }

  cambiarContrasena( form: NgForm ){
    if ( form.invalid ) {
      return;
    }

    let body = {id:this.id, username: this.username, password1: form.value.password1, password2:form.value.password2}

    this._usuarioService.cambiarContrasena(body)
                .subscribe( resp => {

                  if ( resp )
                    this.router.navigate(['/hmoFilter'])
                  else 
                    this.router.navigate(['/changePassword'])
                });


  }

}
