import { Component, OnInit } from '@angular/core';
import { NgwWowService } from 'ngx-wow';
import { Router, NavigationEnd } from '@angular/router';
import { UsuarioService } from 'src/app/services/usuario/usuario.service';
import { Usuario } from 'src/app/models/usuario.model';
import { NgForm } from '@angular/forms';
import { Location } from '@angular/common';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styles: []
})

export class LoginComponent implements OnInit {

  username:string;
  mySubscription: any;
  contador:number = 0;
  entidad:string ='';
  
  constructor(private wowService: NgwWowService,
              public router: Router,
              public _usuarioService: UsuarioService,
              public _location:Location) {
              }

  ngOnInit() {
    this.wowService.init();

  }

  reset() {
    this.wowService.init();
  }

  ingresar( form: NgForm ) {

    if ( form.invalid ) {
      return;
    }

    let usuario = new Usuario(form.value.username, null, null, form.value.password );

    this._usuarioService.login( usuario )
                  .subscribe( resp => {
                    this.entidad = resp.entidad
                    
                    if ( typeof(resp) != 'undefined' ) 
                      if ( resp.first_session )
                        this.router.navigate(['/changePassword'])
                      else 
                        this.router.navigate(['/hmoFilter'])        
                  });

  }
}
