import { Component, OnInit, ChangeDetectorRef, Input } from '@angular/core';
import { NgwWowService } from 'ngx-wow';
import { Chart } from "chart.js";
import 'chartjs-plugin-style';
import { UsuarioService } from 'src/app/services/usuario/usuario.service';
import { Usuario } from 'src/app/models/usuario.model';
import Swal from 'sweetalert2';
import * as $ from 'jquery';
import { FiltroBoardcardsService } from '../filtro-boardcards.service';
import { NgForm } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { codigoLugares, codigoDepartamentos } from 'src/app/diccionarioLugares/dicDepartamentos';
import { formatDate } from '@angular/common';

declare var $: any;
declare var echarts: any;
declare const initFlip: any;
declare const require: any;

@Component({
  selector: 'app-boardcards',
  templateUrl: './boardcards.component.html',
  styles: []
})
export class BoardcardsComponent implements OnInit {

  chartAgeRange: any;

  // FUNCIONALIDAD TABLA GENERAL
  searchFilterGeneral;
  pageActual: number = 1;
  itemsPerPage: number = 10;

  resultados:string[];
  cantidad_devueltos:string;
  cantidad_resultados_descarga:number;

  cantidad_edades_1:string;
  cantidad_edades_2:string;
  cantidad_edades_3:string;
  cantidad_edades_4:string;

  cantidad_genero_1:string;
  cantidad_genero_2:string;

  signo:string[];
  cantidad_signo:string[];

  regimen:string[];
  cantidad_regimen:string[];

  enfermedad:string;
  entidad:string;

  @Input() collection:string;
  @Input() respFiltros:any = {};

  idValidate:string;
  ocultarTablaRespuestas:Boolean = false;
  ocultarSelect:Boolean = true;
  hayResultados:Boolean = true;
  cargando:Boolean = false;

  todosSignos = [];
  paciente;

  fechaEstadoActualizado:any;
  estadoPaciente:string;

  dataEdades = [];

  constructor(private wowService: NgwWowService, public _usuarioService: UsuarioService, private chRef: ChangeDetectorRef, 
              public _actualizarFiltros: FiltroBoardcardsService, private sanitizer: DomSanitizer) {}

  // DATA GRAPH GENDER SIZE

  percentageFemale: number = 60;
  percentageMale: number = 30;

  // DATA GRAPH GENDER TOOLTIP

  numFemale = 3000;
  numMale = 1500;

  cantidadDepartamentosGrafica = 0;
  cantidadMunicipiosGrafica = 0;

  fileUrl;

  graficoEdades;


  ngOnInit() {

    this.enfermedad = sessionStorage.getItem('enfermedad');
    this.entidad = sessionStorage.getItem('entidad');
    this.chRef.detectChanges();
    this.wowService.init();
    initFlip();

  }

  llenarDashboard( respuesta ){

    //Cantidad total de resultados devueltos
    this.cantidad_devueltos = String(respuesta['cantidad_devueltos']);
                  
    //Cantidad de personas en rangos de edades
    this.cantidad_edades_1 = String(respuesta['edades']["0-16"].cantidad);
    this.cantidad_edades_2 = String(respuesta['edades']["17-25"].cantidad);
    this.cantidad_edades_3 = String(respuesta['edades']["26-56"].cantidad);
    this.cantidad_edades_4 = String(respuesta['edades']["57-90"].cantidad);

    //Porcentaje de rangos de edades
    let rango_1 = String(respuesta['edades']["0-16"].porcentaje);
    let rango_2 = String(respuesta['edades']["17-25"].porcentaje);
    let rango_3 = String(respuesta['edades']["26-56"].porcentaje);
    let rango_4 = String(respuesta['edades']["57-90"].porcentaje);

    //Cantidad de personas por genero
    this.cantidad_genero_1 = String(respuesta['genero'].Femenino.cantidad);
    this.cantidad_genero_2 = String(respuesta['genero'].Masculino.cantidad);

    //Porcentaje de personas por genero
    let porcentaje_femenino = String(respuesta['genero'].Femenino.porcentaje);
    let porcentaje_masculino = String(respuesta['genero'].Masculino.porcentaje);

    this.percentageFemale = +porcentaje_femenino;
    this.percentageMale = +porcentaje_masculino;
    

    //Signos
    let signos = respuesta['signos'];
    this.signo = Object.keys(signos);
    this.cantidad_signo = Object.values(signos)

    this.todosSignos = signos;

    //Regimen
    let regimenes = respuesta['regimen'];

    this.regimen = Object.keys(regimenes);
    this.cantidad_regimen = Object.values(regimenes);

    this.llenarTabla(respuesta['resultados']);

    let usuario: Usuario = JSON.parse(sessionStorage.getItem('usuario'));
    this.idValidate = usuario.id;

    this._usuarioService.rolForId(this.idValidate)
      .subscribe( rol => {
        rol = Object.values( rol )[0];
        if ( rol == 'VISITANTE' ){
          this.ocultarTablaRespuestas = true;
        }
        else{
          this.ocultarTablaRespuestas = false;
        }
      });

    setTimeout(function(){
      $('.odometer_one').html( rango_1 );
      $('.odometer_two').html( rango_2 );
      $('.odometer_three').html( rango_3 );
      $('.odometer_four').html( rango_4 );
      $('.odometer_five').html( porcentaje_femenino ) ;
      $('.odometer_six').html( porcentaje_masculino );
      $('.odometer_seven').html( porcentaje_femenino);
      $('.odometer_eight').html( porcentaje_masculino );
    }, 200);
    
    
    this.graficoRangoEdad();
    this.graficoGenero();
    this.graficoRegimen();
    this.graficoSintomas();
    this.mapaUbiacion();
  }

  ngOnChanges(){
    if ( this.respFiltros != null ){
      this.llenarDashboard(this.respFiltros);
    }
  }

  llenarTabla( registros ){
    //Tabla de resultados
    this.resultados = registros;
    if ( typeof(this.resultados[0]) == 'undefined' ){
      this.resultados = [];
      this.ocultarTablaRespuestas = true;
      this.hayResultados = false;
      this.cargando = true;

      // ESCONDE GRAFICA AGE
      let hiddenGraph = document.querySelector(".hiddenGraphAge");
      let attrhiddenGraph = document.createAttribute("style");
      attrhiddenGraph.value = 'display: none !important;';
      hiddenGraph.setAttributeNode(attrhiddenGraph);

      // ESCONDE DATA TABLE AGE
      let hiddenDataTableAge = document.querySelector(".hiddenDataTableAge");
      let attrhiddenDataTableAge = document.createAttribute("style");
      attrhiddenDataTableAge.value = 'display: none !important;';
      hiddenDataTableAge.setAttributeNode(attrhiddenDataTableAge);
  
      // ESCONDE GRAFICA GENDER
      let hiddenGraphGender = document.querySelector(".hiddenGraphGender");
      let attrhiddenGraphGender = document.createAttribute("style");
      attrhiddenGraphGender.value = 'display: none !important;';
      hiddenGraphGender.setAttributeNode(attrhiddenGraphGender);

      // ESCONDE DATA TABLE GENDER
      let hiddenDataTableGender = document.querySelector(".hiddenDataTableGender");
      let attrhiddenDataTableGender = document.createAttribute("style");
      attrhiddenDataTableGender.value = 'display: none !important;';
      hiddenDataTableGender.setAttributeNode(attrhiddenDataTableGender);
  
      // ESCONDE GRAFICA SYMPTOM
      let hiddenGraphSymptom = document.querySelector(".hiddenGraphSymptom");
      let attrhiddenGraphSymptom = document.createAttribute("style");
      attrhiddenGraphSymptom.value = 'display: none !important;';
      hiddenGraphSymptom.setAttributeNode(attrhiddenGraphSymptom);

      // ESCONDE DATA TABLE SYMPTOM
      let hiddenDataTableSymptom = document.querySelector(".hiddenDataTableSymptom");
      let attrhiddenDataTableSymptom = document.createAttribute("style");
      attrhiddenDataTableSymptom.value = 'display: none !important;';
      hiddenDataTableSymptom.setAttributeNode(attrhiddenDataTableSymptom);
  
      // ESCONDE GRAFICA REGIME
      let hiddenGraphRegime = document.querySelector(".hiddenGraphRegime");
      let attrhiddenGraphRegime = document.createAttribute("style");
      attrhiddenGraphRegime.value = 'display: none !important;';
      hiddenGraphRegime.setAttributeNode(attrhiddenGraphRegime);

      // ESCONDE DATA TABLE REGIME
      let hiddenDataTableRegime = document.querySelector(".hiddenDataTableRegime");
      let attrhiddenDataTableRegime = document.createAttribute("style");
      attrhiddenDataTableRegime.value = 'display: none !important;';
      hiddenDataTableRegime.setAttributeNode(attrhiddenDataTableRegime);
    }
    else{
      this.ocultarTablaRespuestas = false;
      this.hayResultados = true;
      this.cargando = false;

      // MUESTRA GRAFICA AGE
      let hiddenGraph = document.querySelector(".hiddenGraphAge");
      let attrhiddenGraph = document.createAttribute("style");
      attrhiddenGraph.value = 'display: block !important;';
      hiddenGraph.setAttributeNode(attrhiddenGraph);

      // ESCONDE DATA TABLE AGE
      let hiddenDataTableAge = document.querySelector(".hiddenDataTableAge");
      let attrhiddenDataTableAge = document.createAttribute("style");
      attrhiddenDataTableAge.value = 'display: block !important;';
      hiddenDataTableAge.setAttributeNode(attrhiddenDataTableAge);
  
      // MUESTRA GRAFICA GENDER
      let hiddenGraphGender = document.querySelector(".hiddenGraphGender");
      let attrhiddenGraphGender = document.createAttribute("style");
      attrhiddenGraphGender.value = 'display: block !important;';
      hiddenGraphGender.setAttributeNode(attrhiddenGraphGender);

      // ESCONDE DATA TABLE GENDER
      let hiddenDataTableGender = document.querySelector(".hiddenDataTableGender");
      let attrhiddenDataTableGender = document.createAttribute("style");
      attrhiddenDataTableGender.value = 'display: block !important;';
      hiddenDataTableGender.setAttributeNode(attrhiddenDataTableGender);
  
      // MUESTRA GRAFICA SYMPTOM
      let hiddenGraphSymptom = document.querySelector(".hiddenGraphSymptom");
      let attrhiddenGraphSymptom = document.createAttribute("style");
      attrhiddenGraphSymptom.value = 'display: block !important;';
      hiddenGraphSymptom.setAttributeNode(attrhiddenGraphSymptom);

      // ESCONDE DATA TABLE SYMPTOM
      let hiddenDataTableSymptom = document.querySelector(".hiddenDataTableSymptom");
      let attrhiddenDataTableSymptom = document.createAttribute("style");
      attrhiddenDataTableSymptom.value = 'display: block !important;';
      hiddenDataTableSymptom.setAttributeNode(attrhiddenDataTableSymptom);
  
      // MUESTRA GRAFICA REGIME
      let hiddenGraphRegime = document.querySelector(".hiddenGraphRegime");
      let attrhiddenGraphRegime = document.createAttribute("style");
      attrhiddenGraphRegime.value = 'display: block !important;';
      hiddenGraphRegime.setAttributeNode(attrhiddenGraphRegime);

      // ESCONDE DATA TABLE REGIME
      let hiddenDataTableRegime = document.querySelector(".hiddenDataTableRegime");
      let attrhiddenDataTableRegime = document.createAttribute("style");
      attrhiddenDataTableRegime.value = 'display: block !important;';
      hiddenDataTableRegime.setAttributeNode(attrhiddenDataTableRegime);
    }
  }

  estado( paciente ){

    this.paciente = paciente;
    //Retornar fechas de los estados del paciente ordenados de mayor a menor
    this.fechaEstadoActualizado = ( Object.values(this.paciente['estado']).sort(function(a,b) {
      return (b['$date']) - (a['$date']);
    }));

    //Saca el estado del paciente con la fecha mas reciente   
    for (let index = 0; index < (Object.keys(this.paciente['estado'])).length; index++) {
      
      if( (Object.values(this.paciente['estado']))[index]['$date'] == this.fechaEstadoActualizado[0]['$date'] ){
        this.estadoPaciente = (Object.keys(this.paciente['estado'])[index]);
      }
    }
    //Se convierte la fecha mas actual formato fecha para ser mostrada
    this.fechaEstadoActualizado = formatDate(new Date(this.fechaEstadoActualizado[0]['$date']), 'MMM dd, y  HH:mm', 'en');

    let idResponsable = JSON.parse( sessionStorage.getItem('usuario') ).id;

    this._usuarioService.rolForId(idResponsable)
      .subscribe( rol => {
        rol = Object.values( rol )[0];
        if ( rol != 'HMO' ){
          this.ocultarSelect = true;
        }
        else{
          this.ocultarSelect = false;
        }
      });
  }

  cambioEstado( form: NgForm ){
    if ( form.invalid ) {
      return;
    }

    $('#modalStateResult').modal('hide');

    Swal.fire({
      title: '¿Esta seguro que desea cambiar el estado de esta persona?',
      text: "Esta acción indica el proceso de tamizaje que tiene actualmente la persona",
      icon: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Nuevo estado: ' + String(Object.values(form.value)[0])
    }).then((result) => {
      if (result.value) {
        let idResponsable = JSON.parse( sessionStorage.getItem('usuario') ).id;
        let entidad = sessionStorage.getItem('entidad');
        let enfermedad = sessionStorage.getItem('enfermedad');
        let collection = enfermedad.toLocaleLowerCase() + '-' + entidad.toUpperCase();
    
        this._usuarioService.verificarUsuarioEntidad( idResponsable, entidad )
          .subscribe( resp => { 
    
            if ( resp.permiso == Boolean(true) ){
    
              this._usuarioService.rolForId(idResponsable)
                .subscribe( rol => {
                  rol = Object.values( rol )[0];
                  if ( rol != 'HMO' ){
                    Swal.fire({
                      icon: 'error',
                      title: 'Error',
                      text: "No tiene permisos para acceder a esta información",
                      allowOutsideClick: false
                    });
          
                    this._usuarioService.logout("");
                  }
                  else{
                    let nuevoEstado = String(Object.values(form.value)[0]).toLowerCase();
                    let idPaciente = (Object.values(this.paciente['_id']))[0];
    
                    form.reset();
                    
                    this._usuarioService.actualizarEstado( idResponsable, idPaciente, nuevoEstado, collection )
                      .subscribe(resp => {
                        
                        //Actualiza el usuario en resultados para actualizar la tabla
                        let bandera = true;
                        let count = 0;
                        while (bandera) {
                          let reg = this.resultados[count];
                          if ( (reg['_id'])['$oid'] == (resp['registro_actualizado'])['_id']['$oid'] ){
                            this.resultados[count] = resp['registro_actualizado'];
                            bandera = false;
                          }
                          count = count + 1;
                        }
    
                        Swal.fire({
                          icon: 'success',
                          title: 'Actualizado',
                          text: 'Estado actualizado con éxito',
                          allowOutsideClick: false
                        });
                      });
                  }
                });
            }
            else{
        
              Swal.fire({
                icon: 'error',
                title: 'Error',
                text: "No tiene permisos para acceder a esta información",
                allowOutsideClick: false
              });
    
              this._usuarioService.logout("");
            }
          });
      }
    })
  }

  public exportExcel( cantidad ): void {

    $('#modalDownloadRecords').modal('hide');

    if (cantidad == 'todos' ) {
      cantidad = this.resultados.length;
    }
    
    let arrayId = [];
    for (let index = 0; index < cantidad; index++) {
      let _id = Object.values(this.resultados[index]['_id'])[0]
      arrayId.push(_id);
    }

    let collection = this.enfermedad.toLowerCase() + "-" + this.entidad.toUpperCase();

    let usuario: Usuario = JSON.parse(sessionStorage.getItem('usuario'));

    this._usuarioService.descargarResultados( usuario.id, collection, arrayId )
      .subscribe( resp => {
      });
  }

  descargarArchivos( form:NgForm ){
    if ( form.invalid ) {
      return;
    }

    this.cantidad_resultados_descarga = form.value.numRegistros;

    form.reset();
    this.exportExcel( this.cantidad_resultados_descarga );
  }

  reset() {
    this.wowService.init();
  }
  
  // GRAPH AGE RANGE

  graficoRangoEdad(){
    this.dataEdades = [];
    this.dataEdades.push({value: this.cantidad_edades_1, name: '0 - 16'});
    this.dataEdades.push({value: this.cantidad_edades_2, name: '17 - 25'});
    this.dataEdades.push({value: this.cantidad_edades_3, name: '26 - 56'});
    this.dataEdades.push({value: this.cantidad_edades_4, name: '57 - 90'});

    var chartIconRangeAge = "data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIj8+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiBpZD0iQ2FwYV8xIiBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCA1MDggNTA4IiBoZWlnaHQ9IjUxMnB4IiB2aWV3Qm94PSIwIDAgNTA4IDUwOCIgd2lkdGg9IjUxMnB4Ij48Zz48Zz48cGF0aCBkPSJtMjU0IDQ2OGMxMTggMCAyMTQtOTYgMjE0LTIxNHMtOTYtMjE0LTIxNC0yMTQtMjE0IDk2LTIxNCAyMTQgOTYgMjE0IDIxNCAyMTR6bTAtNDA4YzEwNi45NzIgMCAxOTQgODcuMDI4IDE5NCAxOTRzLTg3LjAyOCAxOTQtMTk0IDE5NC0xOTQtODcuMDI4LTE5NC0xOTQgODcuMDI4LTE5NCAxOTQtMTk0em0tMTgzIDIyYzAtNS41MjMgNC40NzctMTAgMTAtMTBzMTAgNC40NzcgMTAgMTAtNC40NzcgMTAtMTAgMTAtMTAtNC40NzctMTAtMTB6bTM2Mi42MDUgMzUxLjYwNWMtOTkuMTkzIDk5LjE5My0yNjAuMDE3IDk5LjE5My0zNTkuMjEgMC04Ny45MDktODcuOTA5LTk3LjkwOS0yMjQuMjI0LTMwLjAwMS0zMjMuMTQ3IDMuMTI0LTQuNTU0IDkuMzQ4LTUuNzE0IDEzLjkwMy0yLjU5czUuNzE3IDkuMzUgMi41OTMgMTMuOTA1Yy02Mi41NTIgOTEuMTMyLTUzLjMzNiAyMTYuNzA3IDI3LjY0NyAyOTcuNjkgOTEuMzgzIDkxLjM4MiAyMzkuNTQzIDkxLjM4MyAzMzAuOTI2IDBzOTEuMzgzLTIzOS41NDQgMC0zMzAuOTI2Yy04MS4wNS04MS4wNS0yMDYuNzY0LTkwLjIxNC0yOTcuOTE0LTI3LjQ5My00LjU1MSAzLjEyOS0xMC43NzcgMS45NzYtMTMuOTA2LTIuNTc1cy0xLjk3OC0xMC43OCAyLjU3My0xMy45MDljOTguOTQyLTY4LjA5MSAyMzUuNDA4LTU4LjE0NiAzMjMuMzg5IDI5LjgzNSA5OS4xOTMgOTkuMTkzIDk5LjE5MyAyNjAuMDE3IDAgMzU5LjIxem0tODkuNDQzLTI0MC41MDNjMC01LjUyMiA0LjQ3OC0xMCAxMC0xMGgxNi43NzF2LTE2LjEwMmMwLTUuNTIyIDQuNDc4LTEwIDEwLTEwczEwIDQuNDc4IDEwIDEwdjE2LjEwMmgxNS40MzNjNS41MjIgMCAxMCA0LjQ3OCAxMCAxMHMtNC40NzggMTAtMTAgMTBoLTE1LjQzM3YxNi4xMDJjMCA1LjUyMi00LjQ3OCAxMC0xMCAxMHMtMTAtNC40NzgtMTAtMTB2LTE2LjEwMmgtMTYuNzcxYy01LjUyMiAwLTEwLTQuNDc4LTEwLTEwem0tMTkwLjQ3NCAwYzAtNS41MjIgNC40NzgtMTAgMTAtMTBoMjcuMzM4YzUuNTIyIDAgMTAgNC40NzggMTAgMTB2MTI3Ljg5OGMwIDUuNTIyLTQuNDc4IDEwLTEwIDEwcy0xMC00LjQ3OC0xMC0xMHYtMTE3Ljg5OGgtMTcuMzM4Yy01LjUyMiAwLTEwLTQuNDc4LTEwLTEwem0xMjYuMDgzIDQzLjI0MmMtNC45NTYgMC05LjczNC43NzctMTQuMjIzIDIuMjA2IDEyLjMzLTIzLjg0MiAyOC44OC0zNi41NjggMjkuMTA3LTM2Ljc0IDQuNDMyLTMuMjk2IDUuMzUzLTkuNTYxIDIuMDU2LTEzLjk5MS0zLjI5Ny00LjQzMi05LjgwOS01LjU3Ny0xMy45OTEtMi4wNTYtMjkuNjU2IDI1LjU1OS00OS45MzQgNTkuNDk0LTQ5LjkzNCA5Ny41NjUgMCAyNS45MDcgMjEuMDc3IDQ2Ljk4NCA0Ni45ODQgNDYuOTg0IDI1LjkwOCAwIDQ2Ljk4NS0yMS4wNzcgNDYuOTg1LTQ2Ljk4NC4wMDItMjUuOTA3LTIxLjA3NS00Ni45ODQtNDYuOTg0LTQ2Ljk4NHptMCA3My45NjljLTE0Ljg3OSAwLTI2Ljk4NC0xMi4xMDUtMjYuOTg0LTI2Ljk4NCAwLTE0Ljg4IDEyLjEwNS0yNi45ODUgMjYuOTg0LTI2Ljk4NSAxNC44OCAwIDI2Ljk4NSAxMi4xMDUgMjYuOTg1IDI2Ljk4NS4wMDEgMTQuODc5LTEyLjEwNSAyNi45ODQtMjYuOTg1IDI2Ljk4NHoiIGRhdGEtb3JpZ2luYWw9IiMwMDAwMDAiIGNsYXNzPSJhY3RpdmUtcGF0aCIgZGF0YS1vbGRfY29sb3I9IiMwMDAwMDAiIGZpbGw9IiMyQzJDMkMiLz48L2c+PC9nPiA8L3N2Zz4K"
    let chartRangeAge = echarts.init(document.getElementById('chartRangeAge'));
    chartRangeAge.showLoading();
    chartRangeAge.hideLoading();
    let option = {
        tooltip: {
          trigger: 'item',
          showDelay: 0,
          transitionDuration: 0.2,
          formatter: "{a} <br/>{b} : {c} ({d}%)",
          backgroundColor: '#FFFFFF',
          padding: 5,
          textStyle: {
            color: '#212121',
            fontSize: 13,
            lineHeight:10,
            fontWeight: 'bold',
            fontFamily: 'Helvetica-Light'
          },
          extraCssText: 'box-shadow: 0 0 3px rgba(0, 0, 0, 0.3);'
        },
        graphic: {
          elements: [{
              type: 'image',
              style: {
                  image: chartIconRangeAge,
                  width: 50,
                  height: 50
              },
              left: 'center',
              top: 'center'
          }]
        },
        animation: true,
        animationThreshold: 2000,
        animationDuration: 1800,
        //animationEasing: 'quinticInOut',
        animationDelay: 0,
        animationDurationUpdate: 400,
        //animationEasingUpdate: 'quinticInOut',
        animationDelayUpdate: 0,
        legend: {
            orient: 'vertical',
            left: 15,
            top: 8,
            data:['0 - 16', '17 - 25', '26 - 56', '57 - 90'],
            textStyle: {
              fontWeight: 'bold',
              fontFamily: 'Helvetica-Light'
            }
        },
        toolbox: {
            show: true,
            orient: 'vertical',
            right: 12,
            top: 8,
            itemGap: 5,
            feature: {
                dataView: {
                  show: false,
                  readOnly: false
                },
                restore: {
                  show: false,
                  title: 'Reiniciar Grafica',
                  iconStyle: {
                    borderColor: '#212121',
                    borderWidth: 1
                  },
                  emphasis: {
                    iconStyle: {
                      borderColor: '#109BF8',
                      borderWidth: 1,
                      textFill: '#109BF8',
                      textPadding: 5
                    }
                  }
                },
                saveAsImage: {
                  show: false,
                  title: 'Descargar Imagen',
                  iconStyle: {
                    borderColor: '#212121',
                    borderWidth: 1
                  },
                  emphasis: {
                    iconStyle: {
                      borderColor: '#109BF8',
                      borderWidth: 1,
                      textFill: '#109BF8',
                      textPadding: 5
                    }
                  }
                }
            }
        },
        textStyle: {
          fontWeight: 'bold',
          fontFamily: 'Helvetica-Light'
        },
        color: ['#020175','#184A9C','#2F91C2','#46DAEB'],
        calculable : true,
        series: [
            {
                name:'Rango',
                type:'pie',
                radius: ['30%', '70%'],
                roseType : 'radius',
                hoverOffset: 12,
                emphasis: {
                    label: {
                        show: true
                    }
                },
                label: {
                    normal: {
                        show: true,
                        fontSize: 12,
                        fontWeight: 'bold',
                        fontFamily: 'Helvetica-Light'
                    },
                    emphasis: {
                        show: true,
                        fontSize: 12,
                        fontWeight: 'bold',
                        fontFamily: 'Helvetica-Light'
                    }
                },
                lableLine: {
                    normal: {
                        show: false,
                        fontSize: 12,
                        fontWeight: 'bold',
                        fontFamily: 'Helvetica-Light'
                    },
                    emphasis: {
                        show: true,
                        fontSize: 12,
                        fontWeight: 'bold',
                        fontFamily: 'Helvetica-Light'
                    }
                },
                itemStyle: {
                    normal: {
                        opacity: 1,
                        //shadowBlur: 10,
                        shadowOffsetX: 0,
                        shadowOffsetY: 0,
                        //shadowColor: 'rgba(33, 33, 33, 0.2)'
                    }
                },
                data : this.dataEdades,
            }
        ]
    };

    chartRangeAge.setOption(option);

    $(window).on('resize', function(){
        if(chartRangeAge != null && chartRangeAge != undefined){
            chartRangeAge.resize();
        }
    });
  }
  
  // GRAPH GENDER TOOLTIPS
  graficoGenero(){
    $('.toltip-seguridad-datos').tipso({
      background: '#FFFFFF',
      titleBackground: '',
      speed: 200,
      width: 0,
      maxWidth: '300',
      delay: 100,
      hideDelay: 0,
      animationIn: '',
      animationOut: '',
      offsetX: 0,
      offsetY: -15,
      color: '#212121',
      titleColor: '#212121',
      showArrow: false,
      useTitle: false,
      tooltipHover: true,
      size: 'small',
      position: 'bottom'
    });
  }

  // GRAPH SYMPTOMS AND SIGNS

  graficoSintomas(){  

    let arrayItems:any = [];

    var colorList = [
      [
          '#ff7f50', '#87cefa', '#da70d6', '#32cd32', '#6495ed',
          '#ff69b4', '#ba55d3', '#cd5c5c', '#ffa500', '#40e0d0',
          '#1e90ff', '#ff6347', '#7b68ee', '#d0648a', '#ffd700',
          '#6b8e23', '#4ea397', '#3cb371', '#b8860b', '#7bd9a5'
      ],
      [
          '#ff7f50', '#87cefa', '#da70d6', '#32cd32', '#6495ed',
          '#ff69b4', '#ba55d3', '#cd5c5c', '#ffa500', '#40e0d0',
          '#1e90ff', '#ff6347', '#7b68ee', '#00fa9a', '#ffd700',
          '#6b8e23', '#ff00ff', '#3cb371', '#b8860b', '#30e0e0'
      ],
      [
          '#929fff', '#9de0ff', '#ffa897', '#af87fe', '#7dc3fe',
          '#bb60b2', '#433e7c', '#f47a75', '#009db2', '#024b51',
          '#0780cf', '#765005', '#e75840', '#26ccd8', '#3685fe',
          '#9977ef', '#f5616f', '#f7b13f', '#f9e264', '#50c48f'
      ]
    ][2];
    
    let valMax = Math.max.apply(null, this.cantidad_signo);
    let valMin = Math.min.apply(null, this.cantidad_signo);

    let count = 0;
    for (let index = 0; index < this.signo.length; index++) {
      if (count == 20 ){
        count = 0;
      }
      
      let valor = this.cantidad_signo[index];
      let valNormalizado = ( (+valor - +valMin)/(+valMax - +valMin) );
      arrayItems.push({ "name": this.signo[index], 
                        "value":valor, 
                        "symbolSize": (valNormalizado*100) + 20,
                        "draggable": true,
                        "itemStyle": 
                            {"normal": 
                                {
                                  "shadowBlur": 0,
                                  "shadowColor": colorList[count],
                                  "color": colorList[count]
                                }
                            }
                      });
      count = count + 1;
    }
    
    let valMaxSintoma = 1;
    if ( arrayItems.length > 0 ){
      valMaxSintoma = arrayItems[0]['value'];
    }

    // GRAPH SYMPTOMS AND SIGNS
  
    let chartSymptomsSigns = echarts.init(document.getElementById('chartSymptomsSigns'));
    chartSymptomsSigns.showLoading();
    chartSymptomsSigns.hideLoading();
    let option = {
      tooltip: {
        trigger: 'item',
        showDelay: 0,
        transitionDuration: 0.2,
        formatter: "{a} <br/>{b} : {c}",
        backgroundColor: '#FFFFFF',
        padding: 5,
        textStyle: {
          color: '#212121',
          fontSize: 13,
          lineHeight:10,
          fontWeight: 'bold',
          fontFamily: 'Helvetica-Light'
        },
        extraCssText: 'box-shadow: 0 0 3px rgba(0, 0, 0, 0.3);'
      },
      visualMap: {
          left: 'right',
          min: 1,
          max: valMaxSintoma,
          inRange: {
            color: ['#020175', '#184A9C', '#2F91C2', '#46DAEB', '#E9EBED', '#FD9938', '#EF7841', '#E1574B', '#D23554']
          },
          text: ['Alto', 'Bajo'],
          calculable: true,
          textStyle: {
            color: '#212121',
            fontSize: 12,
            fontWeight: 'bold',
            fontFamily: 'Helvetica-Light'
          }
      },
      animation: true,
      animationThreshold: 2000,
      animationDuration: 1000,
      animationEasing: 'bounceIn',
      animationDelay: 0,
      animationDurationUpdate: 400,
      animationEasingUpdate: 'bounceIn',
      animationDelayUpdate: 0,
      color: ['#fff', '#fff', '#fff'],
      textStyle: {
        fontWeight: 'bold',
        fontFamily: 'Helvetica-Light'
      },
      series: [
          {
            name:'Signo / Sintoma',
            type: 'graph',
            layout: 'force',
            force: {
                repulsion: 350,
                edgeLength: 10,
                gravity: 0.1,
            },
            roam: true,
            label: {
              normal: {
                show: true,
                position: 'top',
                fontSize: 12,
                fontWeight: 'bold',
                fontFamily: 'Helvetica-Light'
              },
              emphasis: {
                show: true,
                position: 'top',
                fontSize: 12,
                fontWeight: 'bold',
                fontFamily: 'Helvetica-Light'
              }
            },
            data: arrayItems
          }
      ]
  };
  
  chartSymptomsSigns.setOption(option);
  
  $(window).on('resize', function(){
      if(chartSymptomsSigns != null && chartSymptomsSigns != undefined){
        chartSymptomsSigns.resize();
      }
  });
  }
  
  // GRAPH REGIME
  graficoRegimen(){
    let chart = new Chart('regimechart', {
      type: 'bar',
      data: {
          labels: this.regimen,
          datasets: [{
              label: 'Dataset 1',
              data: this.cantidad_regimen,
              backgroundColor: "#184A9C",
              borderColor: "#184A9C",
              borderWidth: 0,
              hoverBackgroundColor: "#184A9C",
              hoverBorderColor: "#184A9C"
          }]
      },
      options: {
        events: false,
        legend: { display: false },
        tooltips: {
            enabled: false,
            callbacks: {
              title: function(tooltipItem, data) {
                return data['labels'][tooltipItem[0]['index']];
              },
              label: function(tooltipItem, data) {
                return data['datasets'][0]['data'][tooltipItem['index']];
              }
            },
            backgroundColor: '#FFFFFF',
            titleFontFamily: "'Helvetica-Light', 'Arial', sans-serif",
            titleFontStyle: 'bold',
            titleFontSize: 14,
            titleFontColor: '#212121',
            titleAlign: 'center',
            bodyFontFamily: "'Helvetica-Light', 'Arial', sans-serif",
            bodyFontStyle: 'bold',
            bodyFontSize: 13,
            bodyFontColor: '#212121',
            bodyAlign: 'center',
            xPadding: 8,
            yPadding: 8,
            cornerRadius: 2,
            displayColors: false,
            shadowOffsetX: 3,
            shadowOffsetY: 3,
            shadowBlur: 10,
            shadowColor: 'rgba(33, 33, 33, 0.3)'
          },
          layout: {
              padding: {
                  left: 0,
                  right: 0,
                  top: 15,
                  bottom: 0
              }
          },
          scales: {
              xAxes: [{
                  ticks: {
                      fontSize: 13,
                      fontStyle: 'bold',
                      fontColor: 'rgba(33, 33, 33, 0.5)',
                      fontFamily: "'Helvetica-Light', 'Arial', sans-serif"
                  },
                  stacked: true,
                  gridLines: {
                    borderDash: [3, 3, 3, 3],
                    lineWidth: 1.5,
                    color: "rgba(33, 33, 33, 0)",
                    display: true
                  }
              }],
              yAxes: [{
                ticks: {
                    fontSize: 13,
                    fontStyle: 'bold',
                    fontColor: 'rgba(33, 33, 33, 0.5)',
                    fontFamily: "'Helvetica-Light', 'Arial', sans-serif"
                },
                stacked: true,
                gridLines: {
                  borderDash: [3, 3, 3, 3],
                  lineWidth: 1.5,
                  color: "rgba(33, 33, 33, 0)",
                  display: true
                }
            }]
          },
          animation: {
            duration: 2000,
            animateScale: true,
            animation: true,
            //easing: 'easeInSine',
            onComplete: function () {
              var ctx = this.chart.ctx;
              ctx.textAlign = 'center';
              ctx.textBaseline = 'bottom';
              this.data.datasets.forEach(function (dataset)
              {
                  for (var i = 0; i < dataset.data.length; i++) {
                      for(var key in dataset._meta)
                      {
                          var model = dataset._meta[key].data[i]._model;
                          ctx.fillText(dataset.data[i], model.x - 2, model.y - 5);
                      }
                  }
              });
            }
          },
          responsive: true,
          maintainAspectRatio: false
      }
    });
    
    if ( this.regimen.length == 0 ){
      chart.reset();
    }
  }

  //Mapa de ubicación por departamento y municipio
  mapaUbiacion(){
    let data:any = [];
    let dataDepts:any = [];

    let cantidadMaximaLugares;
    let cantidadMaximaDepts;
    let cantidadMinima;
    
    let codLugares = Object.keys(codigoLugares);
    let codDepts = Object.keys(codigoDepartamentos);

    let arrayCodUser = [];
    let arrayCodDep = [];

    if ( (Object.values(this.respFiltros['resultados']).length) > 0){

      for (const registro of this.respFiltros['resultados']) {
        if ( typeof(registro.ubicacion) != 'undefined' ){
        
          let codMunicipio = String(registro.ubicacion.codigomunicipio);
          let codDepartamento = +(registro.ubicacion.codigodepartamento);
          
          if ( codMunicipio.length == 1 ){
            codMunicipio = `${0}${0}${codMunicipio}`
          }
          if ( codMunicipio.length == 2 ){
            codMunicipio = `${0}${codMunicipio}`
          }
  
          arrayCodDep.push(codDepartamento);
          
          let codLugar = (`${codDepartamento}${codMunicipio}`);
          arrayCodUser.push(codLugar);
        }
      }
  
      this.cantidadDepartamentosGrafica = 0;
      this.cantidadMunicipiosGrafica = 0;
      
      //Data para pintar mapa de calor por departamentos
      codDepts.map(function(codUnique) {
        let count = 0;
        arrayCodDep.forEach(codUser => { 
          if ( String(codUnique) == String(codUser) ){
            count = count + 1;
          }
        });
        if ( count != 0 ){
          dataDepts.push({name: (String(codigoDepartamentos[codUnique])), value:count});
        }
      });
  
      let valoresCantidadDepts = dataDepts.map(function(dato) {
        return +(dato.value);
      });
  
      cantidadMaximaDepts = Math.max.apply(null, valoresCantidadDepts);
      cantidadMinima = 0;
      
      //Data para pintar mapa de calor por municipio
      codLugares.map(function(codUnique) {
        let count = 0;
        arrayCodUser.forEach(codUser => { 
          if ( String(codUnique) == String(codUser) ){
            count = count + 1;
          }
        });
        if ( count != 0 ){
          data.push({name: codUnique, value:count, label:{formatter:String( (codigoLugares[codUnique]) ).split("/")[1]}, tooltip: {
                                    formatter: function (params) {
                                      var value = (params.value + '').split('.');
                                      value[0].replace(/(\d{1,3})(?=(?:\d{3})+(?!\d))/g, '$1,');
                                      return params.seriesName + '<br/>' + String(codigoLugares[codUnique]) + ': ' + value;
                                    }}});
        }
      });
  
      let valoresCantidadLugares = data.map(function(dato) {
        return +(dato.value);
      });
  
      cantidadMaximaLugares = Math.max.apply(null, valoresCantidadLugares);
    }
  
    if ( (data.length != 0) && (dataDepts.length != 0) ){

      this.cantidadDepartamentosGrafica = dataDepts.length;
      this.cantidadMunicipiosGrafica = data.length;

      this.hayResultados = true;

      // GRAPH LOCATION DEPARTMENT
  
      let chartHeathMapLocationDepartment = echarts.init(document.getElementById('chartHeathMapLocationDepartment'));
      chartHeathMapLocationDepartment.showLoading();
      $.get('../../../../assets/data/COLOMBIADEP.json', function (colombiaJson) {
          chartHeathMapLocationDepartment.hideLoading();
          echarts.registerMap('COLOMBIA_DEP', colombiaJson, {});
          let option = {
              tooltip: {
                  trigger: 'item',
                  showDelay: 0,
                  transitionDuration: 0.2,
                  formatter: function (params) {
                      var value = (params.value + '').split('.');
                      value[0].replace(/(\d{1,3})(?=(?:\d{3})+(?!\d))/g, '$1,');
                      return params.seriesName + '<br/>' + params.name + ': ' + value;
                  },
                  backgroundColor: '#FFFFFF',
                  padding: 5,
                  textStyle: {
                    color: '#212121',
                    fontSize: 13,
                    lineHeight:10,
                    fontWeight: 'bold',
                    fontFamily: 'Helvetica-Light'
                  },
                  extraCssText: 'box-shadow: 0 0 3px rgba(0, 0, 0, 0.3);'
              },
              visualMap: {
                  left: 'right',
                  min: cantidadMinima,
                  max: cantidadMaximaDepts,
                  inRange: {
                    color: ['#020175', '#184A9C', '#2F91C2', '#46DAEB', '#E9EBED', '#FD9938', '#EF7841', '#E1574B', '#D23554']
                  },
                  text: ['Alto', 'Bajo'],
                  calculable: true,
                  textStyle: {
                    color: '#212121',
                    fontSize: 12,
                    fontWeight: 'bold',
                    fontFamily: 'Helvetica-Light'
                  }
              },
              toolbox: {
                  show: true,
                  orient: 'vertical',
                  left: 12,
                  top: 8,
                  itemGap: 5,
                  feature: {
                      dataView: {
                        show: false,
                        readOnly: false
                      },
                      restore: {
                        show: false,
                        title: 'Reiniciar Grafica',
                        iconStyle: {
                          borderColor: '#212121',
                          borderWidth: 1
                        },
                        emphasis: {
                          iconStyle: {
                            borderColor: '#109BF8',
                            borderWidth: 1,
                            textFill: '#109BF8',
                            textPadding: 5
                          }
                        }
                      },
                      saveAsImage: {
                        show: false,
                        title: 'Descargar Imagen',
                        iconStyle: {
                          borderColor: '#212121',
                          borderWidth: 1
                        },
                        emphasis: {
                          iconStyle: {
                            borderColor: '#109BF8',
                            borderWidth: 1,
                            textFill: '#109BF8',
                            textPadding: 5
                          }
                        }
                      }
                  }
              },
              textStyle: {
                fontWeight: 'bold',
                fontFamily: 'Helvetica-Light'
              },
              series: [
                  {
                      name: 'Datos Sabio',
                      type: 'map',
                      roam: true,
                      map: 'COLOMBIA_DEP',
                      scaleLimit: {
                        min: 0.8,
                        max: 15,
                      },
                      aspectScale: 1,
                      top: '7%',
                      //layoutCenter: ['48%', '48%'],
                      //layoutSize: 550,
                      emphasis: {
                          label: {
                              show: true
                          }
                      },
                      label: {
                        show: false,
                        color: '#FFFFFF',
                        fontSize: 12,
                        fontWeight: 'bold',
                        fontFamily: 'Helvetica-Light',
                        emphasis: {
                          color: '#FFFFFF',
                          fontSize: 12,
                          fontWeight: 'bold',
                          fontFamily: 'Helvetica-Light'
                        }
                      },
                      itemStyle: {
                        borderColor: '#c4c4c4',
                        borderWidth: 1.3,
                        emphasis: {
                          areaColor: '#FFAB00',
                          borderColor: '#c4c4c4',
                          borderWidth: 1.3
  
                        }
                      },
                      data: dataDepts
                  }
              ]
          };
          chartHeathMapLocationDepartment.setOption(option);
      });
  
      $(window).on('resize', function(){
          if(chartHeathMapLocationDepartment != null && chartHeathMapLocationDepartment != undefined){
              chartHeathMapLocationDepartment.resize();
          }
      });
  
      // GRAPH LOCATION MUNICIPALITY
  
      let chartHeathMapLocationMunicipality = echarts.init(document.getElementById('chartHeathMapLocationMunicipality'));
      chartHeathMapLocationMunicipality.showLoading();
      $.get('../../../../assets/data/COLOMBIAMUNI.json', function (colombiaJson) {
          chartHeathMapLocationMunicipality.hideLoading();
          echarts.registerMap('COLOMBIA_MUNI', colombiaJson, {});
          let option = {
              tooltip: {
                  trigger: 'item',
                  showDelay: 0,
                  transitionDuration: 0.2,
                  backgroundColor: '#FFFFFF',
                  padding: 5,
                  textStyle: {
                    color: '#212121',
                    fontSize: 13,
                    lineHeight:10,
                    fontWeight: 'bold',
                    fontFamily: 'Helvetica-Light'
                  },
                  extraCssText: 'box-shadow: 0 0 3px rgba(0, 0, 0, 0.3);'
              },
              visualMap: {
                  left: 'right',
                  min: cantidadMinima,
                  max: cantidadMaximaLugares,
                  inRange: {
                    color: ['#020175', '#184A9C', '#2F91C2', '#46DAEB', '#E9EBED', '#FD9938', '#EF7841', '#E1574B', '#D23554']
                  },
                  text: ['Alto', 'Bajo'],
                  calculable: true,
                  textStyle: {
                    color: '#212121',
                    fontSize: 12,
                    fontWeight: 'bold',
                    fontFamily: 'Helvetica-Light'
                  }
              },
              toolbox: {
                  show: true,
                  orient: 'vertical',
                  left: 12,
                  top: 8,
                  itemGap: 5,
                  feature: {
                      dataView: {
                        show: false,
                        readOnly: false
                      },
                      restore: {
                        show: false,
                        title: 'Reiniciar Grafica',
                        iconStyle: {
                          borderColor: '#212121',
                          borderWidth: 1
                        },
                        emphasis: {
                          iconStyle: {
                            borderColor: '#109BF8',
                            borderWidth: 1,
                            textFill: '#109BF8',
                            textPadding: 5
                          }
                        }
                      },
                      saveAsImage: {
                        show: false,
                        title: 'Descargar Imagen',
                        iconStyle: {
                          borderColor: '#212121',
                          borderWidth: 1
                        },
                        emphasis: {
                          iconStyle: {
                            borderColor: '#109BF8',
                            borderWidth: 1,
                            textFill: '#109BF8',
                            textPadding: 5
                          }
                        }
                      }
                  }
              },
              textStyle: {
                fontWeight: 'bold',
                fontFamily: 'Helvetica-Light'
              },
              series: [
                  {
                      name: 'Datos Sabio',
                      type: 'map',
                      roam: true,
                      map: 'COLOMBIA_MUNI',
                      scaleLimit: {
                        min: 0.8,
                        max: 15,
                      },
                      aspectScale: 1,
                      top: '7%',
                      //layoutCenter: ['48%', '48%'],
                      //layoutSize: 550,
                      emphasis: {
                          label: {
                              show: true
                          }
                      },
                      label: {
                        show: false,
                        color: '#FFFFFF',
                        fontSize: 12,
                        fontWeight: 'bold',
                        fontFamily: 'Helvetica-Light',
                        emphasis: {
                          color: '#FFFFFF',
                          fontSize: 12,
                          fontWeight: 'bold',
                          fontFamily: 'Helvetica-Light'
                        }
                      },
                      itemStyle: {
                        borderColor: '#c4c4c4',
                        borderWidth: 1.3,
                        emphasis: {
                          areaColor: '#FFAB00',
                          borderColor: '#c4c4c4',
                          borderWidth: 1.3
  
                        }
                      },
                      data: data
                  }
              ]
          };
          chartHeathMapLocationMunicipality.setOption(option);
      });
  
      $(window).on('resize', function(){
          if(chartHeathMapLocationMunicipality != null && this.chartHeathMapLocationMunicipality != undefined){
              chartHeathMapLocationMunicipality.resize();
          }
      });
    }
    else{
      this.hayResultados = false;
      if (this.resultados.length != 0){
        // Swal.fire({
        //   icon:'info',
        //   text: 'Los registros mostrados no poseen ubicación, por tanto el mapa no pudo ser actualizado'
        // });
      }
    }
    }

  cantitieRegister( cantidadRegistros ){
    this.itemsPerPage = cantidadRegistros;
  }
}
