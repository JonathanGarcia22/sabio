import { Component, OnInit } from '@angular/core';
import { UsuarioService } from 'src/app/services/usuario/usuario.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styles: []
})
export class HeaderComponent implements OnInit {

  mostrar_gestion_usuarios:boolean = false;

  constructor(public _usuarioService: UsuarioService) { }

  ngOnInit() {

    this._usuarioService.validaToken(sessionStorage.getItem('token'))
      .subscribe( resp => {
        
        let id:string = JSON.parse( sessionStorage.getItem('usuario') ).id;

        this._usuarioService.rolForId( id )
          .subscribe( rol => {
            rol = Object.values( rol )[0];
            if ( rol == 'ADMIN' ){
              this.mostrar_gestion_usuarios = true;
            }
            else{
              this.mostrar_gestion_usuarios = false;
            }
          });
      });
  }

}
