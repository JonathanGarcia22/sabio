// Module Angular
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

// Module router
import { RouterModule } from '@angular/router';

// Components
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { FilterspanelComponent } from './filterspanel/filterspanel.component';
import { BoardcardsComponent } from './boardcards/boardcards.component';
import { NopagefoundComponent } from './nopagefound/nopagefound.component';

// Modules Angular Download
import { NgwWowModule } from 'ngx-wow';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import {NgxPaginationModule} from 'ngx-pagination';

//Pipes
import { JsonValuesPipe } from '../../pipes/json-values.pipe';
import { ValidateUbicationPipe } from '../../pipes/validate-ubication.pipe';

@NgModule({
    declarations: [
        HeaderComponent,
        FooterComponent,
        FilterspanelComponent,
        BoardcardsComponent,
        NopagefoundComponent,
        JsonValuesPipe,
        ValidateUbicationPipe
    ],
    exports: [
        HeaderComponent,
        FooterComponent,
        FilterspanelComponent,
        BoardcardsComponent,
        NopagefoundComponent
    ],
    imports: [
        RouterModule,
        CommonModule,
        BrowserModule,
        FormsModule,
        NgwWowModule,
        Ng2SearchPipeModule,
        NgxPaginationModule
    ]
})

export class SharedModule { }