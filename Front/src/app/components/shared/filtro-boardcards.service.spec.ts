import { TestBed } from '@angular/core/testing';

import { FiltroBoardcardsService } from './filtro-boardcards.service';

describe('FiltroBoardcardsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FiltroBoardcardsService = TestBed.get(FiltroBoardcardsService);
    expect(service).toBeTruthy();
  });
});
