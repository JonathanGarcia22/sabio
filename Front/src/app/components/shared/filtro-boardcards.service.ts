import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FiltroBoardcardsService {
  
  enfermedades:string[] = [];
  entidades:string[] = [];
  departamentos:string[] = [];
  signos:string[] = [];
  regimen:string[] = [];

  cargandoFilterBoardcards: boolean = true;

  enfermedad_seleccionada:string;
  entidad_seleccionada:string;
  departamento_seleccionado:string;
  rangoEdad_seleccionado:string;
  genero_seleccionado:string;
  signo_seleccionado:string;
  regimen_seleccionado:string;

  gestionUsuario:boolean = false;

  respuesta_enfermedades_departamentos:string[];

  constructor() { }

  public listaEnfermedades( listaEnfermedades:string[] ){
    this.enfermedades = listaEnfermedades;
  }

  public listaEntidades( listaEntidades:string[] ){
    this.entidades = listaEntidades;
  }

  public listaDepartamentos( listaDepartamentos:string[] ){
    for (const place of listaDepartamentos) {
      this.departamentos.push(place.toLowerCase());
    }
  }

  public listaSignos( listaSignos:string[] ){
    this.signos = listaSignos;
  }

  public listaRegimen( listaRegimen:string[] ){
    this.regimen = listaRegimen;
  }

  public enfermedadSeleccionada( enfermedad ){
    this.enfermedad_seleccionada = enfermedad;
  }
  
  public entidadSeleccionada( entidad ){
    this.entidad_seleccionada = entidad;
  }

  public departamentoSeleccionado( departamento ){
    this.departamento_seleccionado = departamento;
  }

  public rangoEdadSeleccionado( rango_edad ){
    this.rangoEdad_seleccionado = rango_edad;
  }

  public generoSeleccionado( genero ){
    this.genero_seleccionado = genero;
  }

  public signoSeleccionado( signo ){
    this.signo_seleccionado = signo;
  }

  public regimenSeleccionado( regimen ){
    this.regimen_seleccionado = regimen;
  }
}
