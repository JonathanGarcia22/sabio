import { Component, OnInit, Input, EventEmitter, Output, HostListener, SimpleChanges, ViewChild, ElementRef, OnChanges } from '@angular/core';
import * as $ from 'jquery';
import { UsuarioService } from 'src/app/services/usuario/usuario.service';
import { Usuario } from 'src/app/models/usuario.model';
import { FiltroBoardcardsService } from '../filtro-boardcards.service';
import { codigoDepartamentos } from 'src/app/diccionarioLugares/dicDepartamentos';
import Swal from 'sweetalert2';
import { fromEvent } from 'rxjs';
import { Renderer2 } from '@angular/core';
import { skipUntil, takeUntil } from 'rxjs/operators';

declare var $: any;
declare const Swiper: any;


@Component({
  selector: 'app-filterspanel',
  templateUrl: './filterspanel.component.html',
  styles: []
})
export class FilterspanelComponent implements OnInit, OnChanges {

  dropdownFilter = null;
  dropdownFilter2 = null;
  dropdownFilter3 = null;
  dropdownFilter4 = null;
  dropdownFilter5 = null;
  dropdownFilter6 = null;
  dropdownFilter7 = null;
  aFiltrosSeleccionados:any[] = [];

  aArray = [
    'pompe', 'fabryh25', 'fabryh16', 'fabrym16','gaucher'
  ];
  aArray2 = [
    'DtscUno', 'DtscDos', 'DtscTres'
  ];
  aArray3 = [
    'DepartamentoUno', 'DepartamentoDos', 'DepartamentoTres'
  ];
  aArray4 = [
    '0 - 16', '17 - 25', '26 - 56', '57 - 90'
  ];
  aArray5 = [
    'Femenino', 'Masculino'
  ];
  aArray6 = [
    'DiscapacidadUno', 'DiscapacidadDos', 'DiscapacidadTres'
  ];
  aArray7 = [
    'RegimenUno', 'RegimenDos', 'RegimenTres'
  ];

  enfermedades:string[] = [];
  entidades:string[] = [];
  departamentos:string[] = [];
  signos:string[] = [];
  regimen:string[] = [];

  enfermedad_seleccionada:string;
  entidad_seleccionada:string;
  departamento_seleccionado:string;
  collection:string;

  cantidad_resultados:string;

  noSignos:Boolean = true;
  noGenero:Boolean = true;
  noRegimen:Boolean = true;
  noEdad:Boolean = true;

  cargando:Boolean = false;

  rangeFilter;
  

  @Input() resp:string[];
  @Input() respVacia;
  @Output() valoresFiltro: EventEmitter<Object[]> = new EventEmitter<Object[]>();

  constructor(public _usuarioService: UsuarioService, public _actualizarFiltros: FiltroBoardcardsService,public renderer: Renderer2) { }

  ngOnInit() {

    this.initSwiperFilter();

    this.cantidad_resultados = this.resp['cantidad_resultados'];
    this.rangeFilter = this.resp['cantidad_devueltos'];

    this.signos = Object.keys(this.resp['signos']);
    this.regimen = Object.keys(this.resp['regimen']);

    this._actualizarFiltros.listaSignos(this.signos);
    this._actualizarFiltros.listaRegimen(this.regimen);

    this.enfermedad_seleccionada = sessionStorage.getItem("enfermedad");
    this.entidad_seleccionada = sessionStorage.getItem("entidad");
    this.departamento_seleccionado = sessionStorage.getItem("departamento");
    this.enfermedades = JSON.parse(sessionStorage.getItem("listaEnfermedades"));
    this.entidades = JSON.parse(sessionStorage.getItem("listaEntidades"));
    this.departamentos = JSON.parse(sessionStorage.getItem("listaDepartamentos"));

    //Nombre de la colección que se consultara para mostrar en el dashboard
    this.collection = this.enfermedad_seleccionada;
    
    $(".js-range-slider").ionRangeSlider({
      skin: "round",
      step: 1,
      grid: true,
      prefix: "",
      to_fixed: true,
      min: 1,
      max: this.cantidad_resultados,
      from: this.rangeFilter,  
    });
    this.rangeFilter = $(".js-range-slider").data("ionRangeSlider");
  }
  
  ngOnChanges(changes: SimpleChanges): void {
    if (  typeof(this.respVacia) != 'undefined' ){
      this.fn_removeFilter(this.respVacia);
    }
  }


  mouseUp(){
    this.valoresFiltro.emit([{'dropdownName': 'numRegistros', 'dropdownValue': this.rangeFilter.old_from}]);
  }

  initSwiperFilter() {
    const swiper = new Swiper('.swiper-container', {
      slidesPerView: 'auto',
      spaceBetween: 15,
      freeMode: true,
      grabCursor: true,
      centeredSlides: false
    });
  }

  onChangeFiltrosPrincipales( filtro ){

    let name_filtro:string = String(Object.values(filtro)[0])
    let valor:string = String(Object.values(filtro)[1])

    if ( name_filtro == 'entidad' ){
      let entitie:string = String(valor).toLowerCase();

      let usuario: Usuario = JSON.parse(sessionStorage.getItem('usuario'));
      let id:string = usuario.id;
      
      this._usuarioService.verificarUsuarioEntidad( id, entitie )
        .subscribe( resp => { 

          if ( resp.permiso == Boolean(true) ){

            this.cargando = true;

            this._usuarioService.cargarEnfermedadesDepartamentos( entitie )
              .subscribe(resp => {
    
                if ( resp == null ){
                  Swal.fire({
                    icon:'info',
                    text: 'No se encontraron resultados para la entidad seleccionada'
                  });
                }
                else{
                  sessionStorage.setItem("listaEnfermedades", JSON.stringify(this.enfermedades).toUpperCase());
                  this.enfermedades = JSON.parse(sessionStorage.getItem('listaEnfermedades'));
          
                  sessionStorage.setItem('enfermedadesDepartamentos', JSON.stringify(resp));
                  
                  this.cargando = false;
                  
                  this.enfermedad_seleccionada = '';
                  this.departamento_seleccionado = '';
                  sessionStorage.setItem('enfermedad', '');
                  sessionStorage.setItem('departamento', '');
                  
                  Swal.fire({
                    icon:'info',
                    text: 'Debe seleccionar una enfermedad'
                  });
                }
                this.cargando = false;
              })
          }
          else{

            Swal.fire({
              icon: 'error',
              title: 'Error',
              text: "No tiene permisos para acceder a esta información",
              allowOutsideClick: false
            });

            this._usuarioService.logout("");
          }
        });
    }
  
    if ( name_filtro == 'enfermedad' ){
  
      sessionStorage.setItem(name_filtro, valor.toUpperCase());
  
      let convertirCodigos = [];
      let respuesta_enfermedades_departamentos = JSON.parse(sessionStorage.getItem('enfermedadesDepartamentos'));
  
      let departamentosMostrar = respuesta_enfermedades_departamentos[valor.toLowerCase()];
  
      //Convertir los codigos de departamento a sus respectivos nombres
      for (let index = 0; index < departamentosMostrar.length; index++) {
        let codigo = departamentosMostrar[index];
        if (codigoDepartamentos[ codigo ] != null){
          convertirCodigos.push(codigoDepartamentos[ codigo ]);
        }
      }
  
      departamentosMostrar = ['TODOS'].concat(convertirCodigos);
      sessionStorage.setItem("listaDepartamentos", JSON.stringify(departamentosMostrar).toUpperCase());
      this.departamentos = JSON.parse(sessionStorage.getItem("listaDepartamentos"));
      
      this.departamento_seleccionado = '';
      sessionStorage.setItem('departamento', '');
      Swal.fire({
        icon:'info',
        text: 'Debe seleccionar un departamento'
      });
    }

    if ( name_filtro == 'departamento' ){
      sessionStorage.setItem(name_filtro, valor.toUpperCase());
      this.valoresFiltro.emit([{'dropdownName': name_filtro,'dropdownValue': valor}]);
    }
  }

  fn_cerrar() {
    this.dropdownFilter = null;
  }

  fn_setFilter( dropdownName:string, dropdownValue ) {

    let aTempFilter = [];
    aTempFilter = this.aFiltrosSeleccionados.filter(function(value, index) {
        return dropdownName !== value.dropdownName;
    });
    aTempFilter.push({'dropdownName': dropdownName,'dropdownValue': dropdownValue});
    this.aFiltrosSeleccionados = aTempFilter

    this.noSignos = false;
    this.noGenero = false;
    this.noRegimen = false;
    this.noEdad= false;   

    if ( (dropdownName == 'entidad') || (dropdownName == 'enfermedad') || (dropdownName == 'departamento') ){
      this.onChangeFiltrosPrincipales( {'dropdownName': dropdownName,'dropdownValue': dropdownValue} );
    }
    //Le notifica al padre sobre los nuevos valores de los filtros
    this.valoresFiltro.emit(this.aFiltrosSeleccionados);
  }

  fn_removeFilter( dropdownName ) {
    this.aFiltrosSeleccionados = this.aFiltrosSeleccionados.filter(function(value, index, arr) {
      return dropdownName !== value.dropdownName;
    });
    this[dropdownName] = null;

    if ( dropdownName == 'signos' ){
      this.noSignos = true;
      this.signos = this._actualizarFiltros.signos;
    }
    if ( dropdownName == 'genero' ){
      this.noGenero = true;
    }
    if ( dropdownName == 'regimen' ){
      this.noRegimen = true;
      this.regimen = this._actualizarFiltros.regimen
    }
    if ( dropdownName == 'edad' ){
      this.noEdad = true;
    };
    this.valoresFiltro.emit(this.aFiltrosSeleccionados);
  }
}
