// Modules Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

// Routs
import { PagesRoutingModule } from './pages.routes';

// Components
import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UsermanagementComponent } from './usermanagement/usermanagement.component';
import { UseractivityComponent } from './useractivity/useractivity.component';

// Components Shared
import { SharedModule } from '../shared/shared.module';

// Modules Angular Download
import { NgwWowModule } from 'ngx-wow';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import {NgxPaginationModule} from 'ngx-pagination';

@NgModule({
    declarations: [
        PagesComponent,
        DashboardComponent,
        UsermanagementComponent,
        UseractivityComponent
    ],
    exports: [
        DashboardComponent,
        UsermanagementComponent,
        UseractivityComponent
    ],
    imports: [
        SharedModule,
        PagesRoutingModule,
        BrowserModule,
        FormsModule,
        CommonModule,
        NgwWowModule,
        Ng2SearchPipeModule,
        NgxPaginationModule,
        NgbModule
    ]
})

export class PagesModule { }