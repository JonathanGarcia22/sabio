import { Component, OnInit } from '@angular/core';
import { NgwWowService } from 'ngx-wow';
import { UsuarioService } from 'src/app/services/usuario/usuario.service';
import { Usuario } from 'src/app/models/usuario.model';
import { FiltroBoardcardsService } from '../../shared/filtro-boardcards.service';
import Swal from 'sweetalert2';
import { codigoDepartamentos } from 'src/app/diccionarioLugares/dicDepartamentos';
//import { TitleCasePipe } from '@angular/common';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styles: []
})
export class DashboardComponent implements OnInit {
  
  resultados:string[] = [];
  quitarFiltro;
  cantidad_resultados:string;
  cantidad_devueltos:string;
  copy_resultados:string[] = null;
  dictPrueba:any = {};
  copy2_resultados:string[] = null;

  enfermedad:string;
  entidad:string;
  collection:string;

  cargando: boolean = true;
  cargar:boolean = true;
  //Porcentaje de registros que traera al cargar la pagina (porcentaje)
  limit:number = 0.3;

  //NuevosValoresFiltro:Object[];

  constructor(private wowService: NgwWowService, public _usuarioService: UsuarioService, public _actualizarFiltros: FiltroBoardcardsService) { }

  ngOnInit() {
    this.wowService.init();

    //comunicacion entre components que no son padre - hijo
    //this._usuarioService.customMessage.subscribe( enfermedad => {
     // this.enfermedad = enfermedad
      //console.log("Dashboard " + this.enfermedad)
    //});

    this.peticionDatosBack();
  }

  reset() {
    this.wowService.init();
  }

  peticionDatosBack(){
    this._usuarioService.validaToken(sessionStorage.getItem('token'))
      .subscribe( resp => {
        console.log('token valido');

        let idValidate:string = JSON.parse( sessionStorage.getItem('usuario') ).id;

        this._usuarioService.rolForId( idValidate )
          .subscribe( rol => {

            rol = Object.values( rol )[0];
            this._usuarioService.usuario.rol = rol;

            //Enfermedad seleccionada
            this.enfermedad = sessionStorage.getItem("enfermedad").toLowerCase();
            //Entidad seleccionada
            this.entidad = sessionStorage.getItem("entidad").toLowerCase();
    
            //Nombre de la colección que se consultara para mostrar en el dashboard
            this.collection = this.enfermedad + '-' + this.entidad.toUpperCase();
    
            let usuario: Usuario = JSON.parse(sessionStorage.getItem('usuario'));
            let id:string = usuario.id;
    
            this.cargando = true;
            this.cargar = true;
            
            this._usuarioService.verificarUsuarioEntidad( id, this.entidad )
              .subscribe( resp => { 
    
                if ( resp.permiso == Boolean(true) ){

                  let ubicacion = sessionStorage.getItem('departamento');
                  
                  let codDepartamento;
                  if ( ubicacion == 'TODOS' ){
                    codDepartamento = 'TODOS';
                  }
                  else{
                    let valores = Object.values(codigoDepartamentos);
                    let index = valores.findIndex( val => val == ubicacion );
                    codDepartamento = +(Object.keys(codigoDepartamentos))[index];
                  }

                  this._usuarioService.cargarResultados( this.collection, codDepartamento, this.limit)
                    .subscribe(resp => {
    
                        this.resultados = resp;
                        this.copy2_resultados = this.resultados;    
                        this.cargando = false;
                        this.cargar = false;
                    });
                  }
                else{
    
                  Swal.fire({
                    icon: 'error',
                    title: 'Error',
                    text: "No tiene permisos para acceder a esta información",
                    allowOutsideClick: false
                  });
    
                  this._usuarioService.logout("");
                }
              });
          });
        });
  }

  getValoresFiltros( filtros ){
    
    //==================================================Nuevos resultados==================================================
    this.copy_resultados = this.resultados['resultados'];

    if ( filtros.length == 0 ){
      this.crearColeccionNuevosResultados( this.copy_resultados, 'No hay filtros' )
    }
    else{
      for (const val of filtros) {
      
        let filtro:string = String(Object.values(val)[0])
        let valor:string = String(Object.values(val)[1])
        
        if ( filtro == 'numRegistros' ){
          this.limit = +valor;
          this.peticionDatosBack();
        }

        //Filtro principales listos
        if ( filtro == 'departamento' ){
          this.limit = 0.3;
          this.peticionDatosBack();
        }
  
        //Filtro para signos
        if ( filtro == 'signos' ){
          this.copy_resultados = this.copy_resultados.filter( reg => Object.values(reg[filtro]).includes(valor));
          this.crearColeccionNuevosResultados( this.copy_resultados, filtro )
        }
  
        //Filtro para edad
        if ( filtro == 'edad' ){
          let limite_inferior = +valor.split("-")[0];
          let limite_superior = +valor.split("-")[1];
          this.copy_resultados = this.copy_resultados.filter(reg => (reg[filtro] >= limite_inferior) && (reg[filtro] <= limite_superior))
          this.crearColeccionNuevosResultados( this.copy_resultados, filtro )
        }
  
        //Filtro para género y régimen
        if ( (filtro == 'genero') || (filtro == 'regimen') ){
          this.copy_resultados = this.copy_resultados.filter( reg => reg[filtro] == valor);
          this.crearColeccionNuevosResultados( this.copy_resultados, filtro )
        }
      }
    }
  }

  crearColeccionNuevosResultados( resultados:string[], nombreFiltro ){

    if ( resultados.length == 0){
      
      Swal.fire({
        title: 'Sin resultados',
        text: 'Seleccione otro filtro o eliminelo de la barra superior',
        icon:'info',
        showCancelButton: false,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'OK',
        allowOutsideClick: false,
        allowEscapeKey: false
      }).then((result) => {
        if (result.value) {
          this.dictPrueba = {}
          this.dictPrueba["edades"] = { "0-16":{"cantidad":0, "porcentaje":0}, "17-25":{"cantidad":0, "porcentaje":0},
                                        "26-56":{"cantidad":0, "porcentaje":0}, "57-90":{"cantidad":0, "porcentaje":0} };
          this.dictPrueba["genero"] = { "Femenino":{"cantidad":0, "porcentaje":0}, "Masculino":{"cantidad":0, "porcentaje":0} };
          this.dictPrueba["signos"] = {};
          this.dictPrueba["regimen"] = {};
          this.dictPrueba["cantidad_devueltos"] = 0;
          this.dictPrueba["resultados"] = {};
      
          this.copy2_resultados = this.dictPrueba;
        }
      })
    }
    else{
      this.cargar= true;
      this._usuarioService.validaToken(sessionStorage.getItem('token'))
        .subscribe( resp => {

          let edades:string[] = []
          let genero:string[] = []
          let regimen:string[] = []
          let signos:string[] = []
      
          for (let i = 0; i < resultados.length; i++) {
            edades.push(resultados[i]['edad']);
            genero.push(resultados[i]['genero']);
            regimen.push(resultados[i]['regimen']);
            signos.push( String( Object.values(resultados[i]['signos']) ) );
          }
      
          //======================================================Edades======================================================
          let rangos = ["0-16","17-25","26-56","57-90"]
      
          let resultado_edades:any = {};
      
          for (const r of rangos) {
            let limite_inferior = r.split("-")[0];
            let limite_superior = r.split("-")[1];
              
            let rango = edades.filter(reg => (reg >= limite_inferior) && (reg <= limite_superior))
            let cantidad_rango = rango.length
            
            resultado_edades[r] = {"cantidad":cantidad_rango, "porcentaje":((cantidad_rango*100/(edades.length)).toFixed(1))}
          }
      
          //======================================================Genero======================================================
          let resultado_genero:any = {};

          for (const gen of genero) {
            if ( (gen.toLowerCase() != "femenino") && (gen.toLowerCase() != "masculino") ){
              let index = genero.indexOf(gen);
              genero.splice(index, 1);
            }
          }    

          const dictGenero = genero.reduce( (contadorGenero, genero) => {
            contadorGenero[genero] = (contadorGenero[genero] ||0) + 1;
            return contadorGenero;
          }, {});
      
          for (let i = 0; i < 2; i++){
            let lista_genero = Object.keys(dictGenero);
            let cantidad_genero = Object.values(dictGenero);
      
            if ( lista_genero.length < 2 ){
              if ( lista_genero[0] == "Femenino" ){
                lista_genero = ["Femenino", "Masculino"];
                cantidad_genero = [cantidad_genero[0], 0];
              }
              else if ( lista_genero[0] == "Masculino" ){
                lista_genero = ["Femenino", "Masculino"];
                cantidad_genero = [0, cantidad_genero[0]];
              }
            }
      
            resultado_genero[lista_genero[i]] = {"cantidad":cantidad_genero[i], "porcentaje":( (+cantidad_genero[i]*100/(genero.length)).toFixed(1))}
          }
      
          //======================================================Signos======================================================
          let resultado_signo:any = {};
          let unpack_list = [];
          
          for (const lista of signos) {
            let list = lista.split(",");
            for (const item of list) {
              unpack_list.push(item); 
            }
          }
      
          signos = unpack_list;
          
          let dictSignos = signos.reduce( (contadorSignos, signo) => {
            contadorSignos[signo] = (contadorSignos[signo] ||0) + 1;
            return contadorSignos;
          }, {});
      
          //Ordenar arreglo
          let dictSignosNombres = Object.keys(dictSignos).sort( (a,b) =>{
            return dictSignos[b] - dictSignos[a];
          });
      
          let dictSignosValores = Object.values(dictSignos).sort( (a,b) =>{
            return +b - +a;
          });
      
          for (let index = 0; index < dictSignosNombres.length; index++) {
            resultado_signo[dictSignosNombres[index]] = +dictSignosValores[index];
          }
      
          //======================================================Regimen======================================================
          let resultado_regimen:any = {};
      
          const dictRegimen = regimen.reduce( (contadorRegimen, regimen) => {
            contadorRegimen[regimen] = (contadorRegimen[regimen] ||0) + 1;
            return contadorRegimen;
          }, {});
          
          resultado_regimen = dictRegimen;
      
          this.dictPrueba = {}
          this.dictPrueba["edades"] = resultado_edades;
          this.dictPrueba["genero"] = resultado_genero;
          this.dictPrueba["signos"] = resultado_signo;
          this.dictPrueba["regimen"] = resultado_regimen;
          this.dictPrueba["cantidad_devueltos"] = resultados.length;
          this.dictPrueba["resultados"] = resultados;
      
          this.copy2_resultados = this.dictPrueba;
          
          this.cargar = false;
        });
    }
  }
}
