import { Component, OnInit } from '@angular/core';
import { NgwWowService } from 'ngx-wow';

@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styles: []
})
export class PagesComponent implements OnInit {

  constructor(private wowService: NgwWowService) {}

  ngOnInit() {
    this.wowService.init();
  }

  reset() {
    this.wowService.init();
  }

}
