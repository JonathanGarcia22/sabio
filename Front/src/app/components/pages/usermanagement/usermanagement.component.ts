import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { NgwWowService } from 'ngx-wow';
import { UsuarioService } from 'src/app/services/usuario/usuario.service'
import { Usuario } from 'src/app/models/usuario.model';
import { NgForm } from '@angular/forms';
import { Router, NavigationEnd } from '@angular/router';
import Swal from 'sweetalert2';
import * as $ from 'jquery';

declare var $: any;

@Component({
  selector: 'app-usermanagement',
  templateUrl: './usermanagement.component.html',
  styles: []
})
export class UsermanagementComponent implements OnInit {

  usuarios:Usuario[];
  //cantidad_usuarios:any;
  itemsPerPage:number = 5;
  p: number = 1;
  previousPage: any;

  entidades:string[];

  mostrarEntidad:boolean = false;
  esconderPassword:boolean = false;

  usuario: Usuario = new Usuario(null,null,null,null,null,null,true,true,null);
  roles:string[] = ["ADMIN", "SANOFI", "HMO", "VISITANTE"];
  modalTitle:string = 'Agregar Usuario';

  successMessage:string;

  // FUNCIONALIDAD TABLA USUARIOS
  searchFilterUser;
  pageActual: number = 1;

  cantidadUsuarios:number;

  password1;
  password2;

  constructor(private wowService: NgwWowService, public _usuarioService: UsuarioService, public router: Router, private chRef: ChangeDetectorRef) {}

  ngOnInit() {
    this.wowService.init();

    this._usuarioService.validaToken(sessionStorage.getItem('token'))
      .subscribe( resp => {

        this._usuarioService.listarUsuarios()
          .subscribe( resp => {
            this.usuarios = resp;
            this.cantidadUsuarios = this.usuarios.length - 1;
          });

        let usuario: Usuario = JSON.parse(sessionStorage.getItem('usuario'));
        let id = usuario.id
        this._usuarioService.listarEntidadesPorUsuario( id )
          .subscribe( resp => {
            let entities:string = String(Object.values(resp));
            this.entidades = entities.split(",");
          });
      });
  }

  reset() {
    this.wowService.init();
  }
  
  guardarUsuario( form: NgForm ){
    if ( form.invalid ) {
      return;
    }
    
    let entidad:string;
    
    if ( (typeof(form.value.entidad) == 'undefined') || (form.value.entidad) == null) {
      entidad = 'todas';
    }
    else{
      entidad = form.value.entidad;
    }

    let idResponsable = JSON.parse( sessionStorage.getItem('usuario') ).id;
    
    //Actualizar usuario
    if ( this.usuario.id != null ){
      this.usuario = {'id':this.usuario.id, 'username':form.value.username, 'first_name': form.value.first_name, 
                'email':form.value.email, 'password':form.value.password, 'rol':form.value.rol.toUpperCase(), 
                'entidad': entidad.toUpperCase(), 'is_active':form.value.is_active}
      this.successMessage = "Usuario actualizado con éxito"
    }
    //Crear usuario
    else{
      this.usuario = {'username':form.value.username, 'first_name': form.value.first_name, 
                'email':form.value.email, 'password':form.value.password, 'rol':form.value.rol.toUpperCase(), 
                'entidad': entidad.toLowerCase(), 'is_active':form.value.is_active}
      this.successMessage = "Usuario creado con éxito"  
    }

    this._usuarioService.guardarUsuario( this.usuario, idResponsable ) 
      .subscribe( usuario => {
        
        Swal.fire({
          icon: 'success',
          text: this.successMessage,
          allowOutsideClick: false
        });

        form.reset();
        $('#modalAddEditUser').modal('hide')

        this._usuarioService.listarUsuarios()
          .subscribe( resp => {
            this.usuarios = resp;
            this.cantidadUsuarios = this.usuarios.length - 1;
          });
      });
  }

  onChangeRol( rol ) {
    if ( (rol.toLowerCase() == 'admin') || (rol.toLowerCase() == 'sanofi') || (rol.toLowerCase() == 'visitante') ){
      this.mostrarEntidad = false;
    }
    else{
      this.mostrarEntidad = true;
    }
  }  

  cantitieUser( cantidadUsuarios ){
    this.itemsPerPage = cantidadUsuarios;
  }

  agregar(){
    this.modalTitle = "Agregar Usuario";
    this.esconderPassword = false;
    this.usuario = {'username':null,'first_name':null,'email':null,'password':null,'rol':null,'entidad':null,'is_active':true}
  }

  editar( user ){
    this.modalTitle = 'Editar Usuario';
    this.esconderPassword = true;
    this.usuario = user;
    this.onChangeRol( this.usuario.rol )
  }

  cambiarPassword( form:NgForm ){
    if ( form.invalid ) {
      return;
    }

    this._usuarioService.validaToken(sessionStorage.getItem('token'))
      .subscribe( resp => {

        let body = {id:this.usuario.id, username: this.usuario.username, password1: form.value.password1, password2:form.value.password2}

        form.reset();
        $('#modalChangePassword').modal('hide')
    
        this._usuarioService.cambiarContrasena(body)
          .subscribe( resp => {

            if ( resp )
              Swal.fire({
                icon: 'success',
                text: 'Contraseña cambiada con éxito',
                allowOutsideClick: false
              });
              
              form.reset();

              this._usuarioService.listarUsuarios()
                .subscribe( resp => {
                  this.usuarios = resp;
                  this.cantidadUsuarios = this.usuarios.length - 1;
                });
          });
      });
  }
}

