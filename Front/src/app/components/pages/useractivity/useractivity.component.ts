import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { NgwWowService } from 'ngx-wow';
import { Chart } from "chart.js";
import { UsuarioService } from 'src/app/services/service.index';
import Swal from 'sweetalert2';
import { Usuario } from 'src/app/models/usuario.model';

declare var $: any;
declare var echarts: any;

@Component({
  selector: 'app-useractivity',
  templateUrl: './useractivity.component.html',
  styles: []
})
export class UseractivityComponent implements OnInit {

  chart: any;

  datos = [];
  labelGrafica = [];
  dataGrafica = [];

  usuarios;

  pageActual: number = 1;

  constructor(private wowService: NgwWowService, private chRef: ChangeDetectorRef, public _usuarioService: UsuarioService) { }

  ngOnInit() {
    this.wowService.init();

    this._usuarioService.validaToken(sessionStorage.getItem('token'))
      .subscribe(resp => {

        this._usuarioService.listarUsuarios()
          .subscribe( resp => {
            this.usuarios = resp;

            let calculeDataCards = ['last_login', 'date_joined'];
            let creadosLogeadosUltimoMes = {};
            let creadosLogeadosUltimaSemana = {};
            
            for (const item of calculeDataCards) {
              let arrayFechas = this.usuarios.map(function (reg){
                return reg[item];
              });
              arrayFechas = arrayFechas.filter(fecha => fecha != null);
              let hoy = Date.now();
              let activosMes = [];
              let activoSemana = [];
              let semanaEnMilisegundos = 1000 * 60 * 60 * 24 * 7;
              let mesEnMilisegundos = 1000 * 60 * 60 * 24 * 30;
              let fechaHaceUnaSemana;
              let fechaHaceUnMes;
              for (const fecha of arrayFechas) {
                if ( (new Date(hoy).getFullYear() == new Date(fecha).getFullYear()) ){
                  let restaSemana = new Date(hoy).getTime() - semanaEnMilisegundos; //getTime devuelve milisegundos de esa fecha
                  fechaHaceUnaSemana = new Date(restaSemana);    //Fecha hace exactamente una semana
                  if ( fechaHaceUnaSemana < new Date(fecha) ){
                    activoSemana.push(fecha);
                  }
    
                  let restaMes = new Date(hoy).getTime() - mesEnMilisegundos;
                  fechaHaceUnMes = new Date(restaMes);          //Fecha hace exactamente un mes
                  if ( fechaHaceUnMes < new Date(fecha)){
                    activosMes.push(fecha);
                  }
                } 
              }
              creadosLogeadosUltimoMes[item] = activosMes.length;
              creadosLogeadosUltimaSemana[item] = activoSemana.length; 
            }

            this._usuarioService.listarLogsDescargas()
              .subscribe(resp => {
                this.datos = resp['logsDescargas'];
                this.labelGrafica = (resp['labelEnfermedad']);
                this.dataGrafica = resp['dataEnfermedad'];
    
                this.chRef.detectChanges();
                this.chartActivityCardOne();
                this.chartActivityCardTwo();
                this.chartActivityCardThree();
                this.chartActivityCardFour();
                this.chartDisease();

                setTimeout(function () {
                  $('.resultActivityOne').html(creadosLogeadosUltimoMes['last_login']);
                  $('.resultActivityTwo').html(creadosLogeadosUltimaSemana['last_login']);
                  $('.resultActivityThree').html(creadosLogeadosUltimoMes['date_joined']);
                  $('.resultActivityFour').html(creadosLogeadosUltimaSemana['date_joined']);
                }, 200);
              });
          });
      });
  }

  reset() {
    this.wowService.init();
  }

  borrarArchivos(){
    this._usuarioService.listarArchivosBorrar()
      .subscribe(resp => {
       let cantidadBorrar = resp['nombres'].length;

       if ( cantidadBorrar > 0 ){
        Swal.fire({
          title: `Hay ${cantidadBorrar} nuevos archivos para borrar`,
          text: "Estos archivos ya fueron enviados al correo de quien los solicitó, al borrarlos optimizará espacio en disco",
          icon: 'info',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Si. Borrar'
        }).then((result) => {
          if (result.value) {
    
            let id = JSON.parse(sessionStorage.getItem('usuario')).id;
            this._usuarioService.borrarLogsDescargas(id)
              .subscribe(resp => {
    
                Swal.fire(
                  'Borrado',
                  'Archivos borrados con éxito',
                  'success'
                )
              });
          }
        })
       }
       else{
        Swal.fire({
          text: "No hay archivos nuevos",
          icon: 'info',
          confirmButtonText: 'OK'
        });
       }
    });
  }

  chartActivityCardOne() {

    this.chart = new Chart('ChartActivityCardOne', {
        type: 'line',
        data: {
          labels: ["step", "step", "step", "step", "step", "step", "step", "step", "step", "step", "step", "step", "step"],
            datasets: [{
            data: [20, 55, 80, 30, 15, 30, 45, 50, 64, 72, 84, 85, 50, 200],
            label: '# of Votes',
            fill: false,
            lineTension: 0.4,
            backgroundColor: "rgba(93,120,255,1)",
            borderColor: "rgba(93,120,255,1)",
            borderCapStyle: 'round',
            borderWidth: 3,
            borderJoinStyle: 'round',
            pointBorderWidth: 0,
            pointRadius: 0,
            pointHitRadius: 0,
            spanGaps: false
            }]
        },
        options: {
            legend: { display: false },
            scales: {
                xAxes: [{
                    ticks: {
                        display: false,
                        fontSize: 13,
                        fontStyle: 'bold',
                        fontColor: 'rgba(33, 33, 33, 0.5)',
                        fontFamily: "'Helvetica-Light', 'Arial', sans-serif"
                    },
                    stacked: false,
                    gridLines: {
                      borderDash: [3, 3, 3, 3],
                      lineWidth: 1.5,
                      color: "rgba(33, 33, 33, 0)",
                      display: false
                    }
                }],
                yAxes: [{
                  ticks: {
                      display: false,
                      fontSize: 13,
                      fontStyle: 'bold',
                      fontColor: 'rgba(33, 33, 33, 0.5)',
                      fontFamily: "'Helvetica-Light', 'Arial', sans-serif"
                  },
                  stacked: false,
                  gridLines: {
                    borderDash: [3, 3, 3, 3],
                    lineWidth: 1.5,
                    color: "rgba(33, 33, 33, 0)",
                    display: false
                  }
              }]
            },
            animation: {
              duration: 1500,
              animateScale: true,
              animation: true,
              easing: 'easeOutQuad',
            },
            responsive: true,
            maintainAspectRatio: false
        }
    });
  }

  chartActivityCardTwo() {

    this.chart = new Chart('ChartActivityCardTwo', {
        type: 'line',
        data: {
          labels: ["step", "step", "step", "step", "step", "step", "step", "step", "step", "step", "step", "step", "step"],
            datasets: [{
            data: [20, 23, 55, 15, 8, 15, 40, 25, 80, 80, 80, 80, 60, 60, 200],
            label: '# of Votes',
            fill: false,
            lineTension: 0.4,
            backgroundColor: "rgba(253,57,149,1)",
            borderColor: "rgba(253,57,149,1)",
            borderCapStyle: 'round',
            borderWidth: 3,
            borderJoinStyle: 'round',
            pointBorderWidth: 0,
            pointRadius: 0,
            pointHitRadius: 0,
            spanGaps: true
            }]
        },
        options: {
            legend: { display: false },
            scales: {
                xAxes: [{
                    ticks: {
                        display: false,
                        fontSize: 13,
                        fontStyle: 'bold',
                        fontColor: 'rgba(33, 33, 33, 0.5)',
                        fontFamily: "'Helvetica-Light', 'Arial', sans-serif"
                    },
                    stacked: false,
                    gridLines: {
                      borderDash: [3, 3, 3, 3],
                      lineWidth: 1.5,
                      color: "rgba(33, 33, 33, 0)",
                      display: false
                    }
                }],
                yAxes: [{
                  ticks: {
                      display: false,
                      fontSize: 13,
                      fontStyle: 'bold',
                      fontColor: 'rgba(33, 33, 33, 0.5)',
                      fontFamily: "'Helvetica-Light', 'Arial', sans-serif"
                  },
                  stacked: false,
                  gridLines: {
                    borderDash: [3, 3, 3, 3],
                    lineWidth: 1.5,
                    color: "rgba(33, 33, 33, 0)",
                    display: false
                  }
              }]
            },
            animation: {
              duration: 1600,
              animateScale: true,
              animation: true,
              easing: 'easeOutQuad',
            },
            responsive: true,
            maintainAspectRatio: false
        }
    });
  }

  chartActivityCardThree() {

    this.chart = new Chart('ChartActivityCardThree', {
        type: 'line',
        data: {
          labels: ["step", "step", "step", "step", "step", "step", "step", "step", "step", "step", "step", "step", "step"],
            datasets: [{
            data: [40, 10, 80, 30, 15, 20, 45, 50, 75, 72, 84, 85, 100, 200],
            label: '# of Votes',
            fill: false,
            lineTension: 0.4,
            backgroundColor: "rgba(255,171,0,1)",
            borderColor: "rgba(255,171,0,1)",
            borderCapStyle: 'round',
            borderWidth: 3,
            borderJoinStyle: 'round',
            pointBorderWidth: 0,
            pointRadius: 0,
            pointHitRadius: 0,
            spanGaps: true
            }]
        },
        options: {
            legend: { display: false },
            scales: {
                xAxes: [{
                    ticks: {
                        display: false,
                        fontSize: 13,
                        fontStyle: 'bold',
                        fontColor: 'rgba(33, 33, 33, 0.5)',
                        fontFamily: "'Helvetica-Light', 'Arial', sans-serif"
                    },
                    stacked: false,
                    gridLines: {
                      borderDash: [3, 3, 3, 3],
                      lineWidth: 1.5,
                      color: "rgba(33, 33, 33, 0)",
                      display: false
                    }
                }],
                yAxes: [{
                  ticks: {
                      display: false,
                      fontSize: 13,
                      fontStyle: 'bold',
                      fontColor: 'rgba(33, 33, 33, 0.5)',
                      fontFamily: "'Helvetica-Light', 'Arial', sans-serif"
                  },
                  stacked: false,
                  gridLines: {
                    borderDash: [3, 3, 3, 3],
                    lineWidth: 1.5,
                    color: "rgba(33, 33, 33, 0)",
                    display: false
                  }
              }]
            },
            animation: {
              duration: 1700,
              animateScale: true,
              animation: true,
              easing: 'easeOutQuad',
            },
            responsive: true,
            maintainAspectRatio: false
        }
    });
  }

  chartActivityCardFour() {

    this.chart = new Chart('ChartActivityCardFour', {
        type: 'line',
        data: {
          labels: ["step", "step", "step", "step", "step", "step", "step", "step", "step", "step", "step", "step", "step"],
            datasets: [{
            data: [50, 55, 20, 30, 15, 30, 75, 75, 64, 56, 84, 65, 70, 200],
            label: '# of Votes',
            fill: false,
            lineTension: 0.4,
            backgroundColor: "rgba(59,194,166,1)",
            borderColor: "rgba(59,194,166,1)",
            borderCapStyle: 'round',
            borderWidth: 3,
            borderJoinStyle: 'round',
            pointBorderWidth: 0,
            pointRadius: 0,
            pointHitRadius: 0,
            spanGaps: true
            }]
        },
        options: {
            legend: { display: false },
            scales: {
                xAxes: [{
                    ticks: {
                        display: false,
                        fontSize: 13,
                        fontStyle: 'bold',
                        fontColor: 'rgba(33, 33, 33, 0.5)',
                        fontFamily: "'Helvetica-Light', 'Arial', sans-serif"
                    },
                    stacked: false,
                    gridLines: {
                      borderDash: [3, 3, 3, 3],
                      lineWidth: 1.5,
                      color: "rgba(33, 33, 33, 0)",
                      display: false
                    }
                }],
                yAxes: [{
                  ticks: {
                      display: false,
                      fontSize: 13,
                      fontStyle: 'bold',
                      fontColor: 'rgba(33, 33, 33, 0.5)',
                      fontFamily: "'Helvetica-Light', 'Arial', sans-serif"
                  },
                  stacked: false,
                  gridLines: {
                    borderDash: [3, 3, 3, 3],
                    lineWidth: 1.5,
                    color: "rgba(33, 33, 33, 0)",
                    display: false
                  }
              }]
            },
            animation: {
              duration: 1800,
              animateScale: true,
              animation: true,
              easing: 'easeOutQuad',
            },
            responsive: true,
            maintainAspectRatio: false
        }
    });
  }

  chartDisease() {

    var chartIconDisease = "data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIj8+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiBpZD0iQ2FwYV8xIiBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCA0OTkuNTczIDQ5OS41NzMiIGhlaWdodD0iNTEycHgiIHZpZXdCb3g9IjAgMCA0OTkuNTczIDQ5OS41NzMiIHdpZHRoPSI1MTJweCIgY2xhc3M9ImhvdmVyZWQtcGF0aHMiPjxnPjxnPjxwYXRoIGQ9Im00OTkuNDIxIDIyOC45NjhjLTEuMDQzLTEzLjU0LTcuMTY0LTI1LjYzOS0xNy4yMzMtMzQuMDY5LTkuNDI2LTcuODkxLTIxLjI3MS0xMS41NjktMzMuMzU2LTEwLjM1Ny0xNi40ODIgMS42NTMtMzAuOTk5IDEyLjQzNS0zOC4xNTUgMjcuOTU3bC0xNC45NzkgMS4zMjMtMTQuNzg0LTQuMTQ0Yy0xLjE0OS0uMzIyLTIuMDc2LTEuMjMyLTIuNDgtMi40MzQtNC4yOTYtMTIuNzgxLTEwLjQyOC0yNC44NS0xOC4yNDEtMzUuOTA1bDUuMjMyLTE0LjA2NyAxNi41MTMtMTcuODZjOS4zOTItLjMxNSAxOC4xMjYtNC40NTUgMjQuMzg5LTExLjY3NCAxMS4zMzMtMTMuMDYzIDEwLjg2Mi0zMy4wODItMS4wNzQtNDUuNTc1LTYuNTUxLTYuODU2LTE1LjM3NS0xMC42MTQtMjQuODEzLTEwLjUxMy05LjMxOS4wNzYtMTguMjkgNC4wNDItMjQuNjE0IDEwLjg4My02LjE5NSA2LjctOS4zNjcgMTUuNDM5LTguOTczIDI0LjQzNWwtMTYuNTE1IDE3Ljg2My0xMy44MzggNi40MTljLTIwLjY0LTEyLjA2NS00NC4wNzgtMTguNTktNjguMDUyLTE4LjkzOGwtOS4wNDgtMTQuNDMtMy4xOTEtMTUuODU3YzEyLjcxMy0xMC4zOTUgMTguNjUzLTI2LjU0MSAxNS4yNTYtNDIuNDQ3LTUuMDk4LTIzLjg2OS0yOS42NzctMzkuNDE3LTU0Ljc4Ni0zNC42NTQtMTIuNjQyIDIuMzk2LTIzLjM5OCA5LjQyNi0zMC4yODcgMTkuNzkyLTYuNDUzIDkuNzExLTguNjgyIDIxLjI2NC02LjI3NSAzMi41MyAzLjIwOCAxNS4wMjEgMTQuMDk3IDI3LjEzOCAyOC45MTEgMzIuNTQ0bDMuNTI1IDE3LjUxOC0yLjgwNCAxNy4wNzVjLTEyLjc3MiA1LjcxMy0yNC41NTEgMTMuMjg3LTM1LjEwNCAyMi41N2wtMTAuMzcyLTEuODQ4LTExLjcyLTguODQyYy0xLjY3NS04LjkwMS02LjgxMy0xNi43MzctMTQuNDQ3LTIxLjg1OS0xNC4yNDctOS41Ni0zMy40MDUtNi45NDUtNDQuNTYzIDYuMDgyLTYuMTY4IDcuMjAyLTkuMDA0IDE2LjM0My03Ljk4NSAyNS43MzcgMS4wMDUgOS4yNjQgNS44NDcgMTcuNzk1IDEzLjI4NCAyMy40MDYgNy42OSA1LjgwMSAxNy40NyA4LjAxNCAyNi44MzEgNi4yMTVsOS44MjEgNy40MDkgNC40MDUgOS4wMTRjLjE5NS4zOTkuMi44NTkuMDE1IDEuMjYxLTguNDAxIDE4LjI2Ny0xMi42NiAzNy43NjktMTIuNjYgNTcuOTY1IDAgMy43MzguMTU1IDcuNTM4LjQ2IDExLjI5NC4wNDcuNTgtLjE4MiAxLjExLS42MTQgMS40MTdsLTE2LjU4NSAxMS44MTctMTguOTMgNS45NTVjLTExLjYyMy04Ljk5My0yNi45OTItMTEuNzA5LTQxLjEzOC03LjAzNC0uMjgzLjA5NC0uNTYyLjE5NS0uODQuMjk4bC0uMjM0LjA4NmMtMy44OTcgMS40MDQtNS45MTggNS43MDEtNC41MTUgOS41OTggMS40MDMgMy44OTYgNS42OTggNS45MTkgOS41OTggNC41MTVsLjM1MS0uMTI4Yy4xMTgtLjA0My4yMzYtLjA4OC4zNTMtLjEyNyAxMC4zMzEtMy40MTYgMjEuNjUxLS44NzUgMjkuNTQ0IDYuNjMgMS40MTUgMS4zNDUgMy4yNzIgMi4wNjQgNS4xNjggMi4wNjQuNzUyIDAgMS41MTEtLjExMyAyLjI1LS4zNDZsMjQuMDI4LTcuNTU5Yy43NTEtLjIzNiAxLjQ2LS41ODkgMi4xMDItMS4wNDZsMTcuNTUyLTEyLjUwNmM0LjcwNi0zLjM1MiA3LjMzNS05LjA0MyA2Ljg2MS0xNC44NTItLjI3Mi0zLjM1My0uNDExLTYuNzQzLS40MTEtMTAuMDc3IDAtMTguMDE3IDMuNzk4LTM1LjQxMSAxMS4yODktNTEuNjk4IDIuMDczLTQuNTA5IDIuMDEzLTkuNjUzLS4xNjYtMTQuMTEzbC01LjE5NC0xMC42MjhjLS41MTgtMS4wNjEtMS4yNzktMS45ODMtMi4yMjEtMi42OTRsLTE0LjExOS0xMC42NTJjLTEuOTQ2LTEuNDY4LTQuNDg1LTEuODk5LTYuODA0LTEuMTU1LTUuODU2IDEuODc2LTEyLjI3MS43OTItMTcuMTYyLTIuODk4LTQuMjA3LTMuMTczLTYuODM2LTcuODA4LTcuNDA1LTEzLjA1LS41NjgtNS4yMzYgMS4wMTgtMTAuMzM2IDQuNDY1LTE0LjM2MSA2LjExLTcuMTMzIDE3LjAwOS04LjYyIDI0LjgxMy0zLjM4NCA0LjgxMSAzLjIyOSA3Ljg0NyA4LjM4MyA4LjMzIDE0LjE0My4xNzggMi4xMjYgMS4yNTMgNC4wNzYgMi45NTcgNS4zNjFsMTUuNjM4IDExLjc5OGMuOTQyLjcxMSAyLjAzOSAxLjE4OSAzLjIwMSAxLjM5NmwxNS43NTYgMi44MDhjMi4zMTkuNDE0IDQuNjkzLS4yODYgNi40MTctMS44ODYgMTAuOTc3LTEwLjE4NiAyMy41MjMtMTguMjI0IDM3LjI5Mi0yMy44OTEgMi40MDUtLjk5IDQuMTI1LTMuMTU0IDQuNTQ2LTUuNzJsMy42OTItMjIuNDhjLjE0Ny0uODk0LjEzLTEuODA3LS4wNDgtMi42OTVsLTQuNjcxLTIzLjIxNWMtLjU2Ny0yLjgxOS0yLjY5Ni01LjA2Ni01LjQ4MS01Ljc4My0xMS41OTgtMi45ODgtMjAuMjU0LTExLjY0MS0yMi41OS0yMi41OC0xLjU1Ni03LjI4NC0uMS0xNC43NzYgNC4xLTIxLjA5NiA0LjYzNS02Ljk3NSAxMS45NDctMTEuNzE5IDIwLjU4OC0xMy4zNTcgMTcuMTczLTMuMjUyIDMzLjkxNCA3LjA4NSAzNy4zMjMgMjMuMDQ5IDIuNDQxIDExLjQzMi0yLjY0NSAyMy4xMjctMTIuOTU4IDI5Ljc5NS0yLjU4NyAxLjY3My0zLjg4OCA0Ljc1OC0zLjI4IDcuNzc4bDQuNDA3IDIxLjljLjE3OS44ODguNTE3IDEuNzM2Ljk5OCAyLjUwNGwxMC44MzMgMTcuMjgxYzIuMDkyIDMuMzM3IDUuNjkxIDUuMzI5IDkuNjQ3IDUuMzI5aC4wMDRjMjIuNTc0IDAgNDQuNjk2IDYuMTUyIDYzLjk3NyAxNy43OTIgMy4yMzIgMS45NTEgNy4xOTkgMi4xNzIgMTAuNjExLjU5MWwxNi45NTctNy44NjZjLjg4OC0uNDEyIDEuNjg3LS45OTQgMi4zNTEtMS43MTJsMTkuOC0yMS40MTZjMS41NTQtMS42OCAyLjI1NS0zLjk3NyAxLjkwNS02LjIzOS0uODg5LTUuNzQ0Ljg1Ny0xMS40NzggNC43OS0xNS43MzEgMy41NzctMy44NjkgOC40NTEtNi4wMjMgMTMuNzIzLTYuMDY3IDUuMjUyLS4wMDEgMTAuMTg0IDIuMDQzIDEzLjg0NSA1Ljg3NiA2LjY0NSA2Ljk1NSA2LjkwNCAxOC4xMDQuNTg5IDI1LjM4Mi0zLjk2IDQuNTY0LTkuNjkgNi45Mi0xNS43MjIgNi40NjctMi4yODEtLjE3My00LjUxOC43MDgtNi4wNyAyLjM4OGwtMTkuOCAyMS40MTZjLS42NjUuNzE4LTEuMTgyIDEuNTYtMS41MjMgMi40NzdsLTYuNDM3IDE3LjMwOGMtMS4zMjMgMy41NTctLjc3MiA3LjUyMSAxLjQ3MiAxMC42MDQgNy40NjEgMTAuMjUgMTMuMjgxIDIxLjUwNiAxNy4yOTggMzMuNDU3IDEuOTg3IDUuOTEzIDYuNzE2IDEwLjQzNiAxMi42NTEgMTIuMDk5bDE2LjEwMyA0LjUxM2MuODcyLjI0NSAxLjc4MS4zMjggMi42ODQuMjQ5bDIwLjkxMy0xLjg0N2MyLjk0Mi0uMjYgNS40NTctMi4yMjEgNi40MjYtNS4wMTEgNC4zMy0xMi40NzMgMTUuMDYxLTIxLjMyOCAyNy4zMzgtMjIuNTU5IDguMDI5LS44MDYgMTUuOTI3IDEuNjU3IDIyLjIzMSA2LjkzNCA2Ljk0NyA1LjgxNiAxMS4xNzYgMTQuMjQgMTEuOTA2IDIzLjcxOS40ODIgNi4yNjQtLjY1IDEyLjU1NC0zLjI3NCAxOC4xOTEtMS43NDggMy43NTUtLjEyMiA4LjIxNiAzLjYzNCA5Ljk2NSAzLjc1NyAxLjc0OCA4LjIxNy4xMjIgOS45NjUtMy42MzQgMy43MDUtNy45NyA1LjMwNi0xNi44NDggNC42MjctMjUuNjc2eiIgZGF0YS1vcmlnaW5hbD0iIzAwMDAwMCIgY2xhc3M9ImhvdmVyZWQtcGF0aCBhY3RpdmUtcGF0aCIgZGF0YS1vbGRfY29sb3I9IiMwMDAwMDAiIGZpbGw9IiMyQzJDMkMiLz48cGF0aCBkPSJtNDYxLjY3NCAyNjYuMTA1Yy0yLjEwNS42OTMtNC4yNzcgMS4xNTUtNi40NTUgMS4zNzQtMTIuOTc5IDEuMjk5LTI1LjExLTUuODg4LTMwLjkxNy0xOC4zMTUtMS4zMzUtMi44NTYtNC4zMTYtNC41NzUtNy40NTUtNC4yOTVsLTE5LjYzMiAxLjczNGMtLjkwMy4wOC0xLjc4My4zMjItMi41OTkuNzE2bC0xNC44MTkgNy4xNDljLTUuNjE0IDIuNzA4LTkuNDg4IDguMDU4LTEwLjM2MSAxNC4zMS0xLjMgOS4zMDMtMy42NjkgMTguNDU5LTcuMDQgMjcuMjEyLTIuMDEyIDUuMjI1LTEuNjE0IDEwLjk5OCAxLjA5MyAxNS44MzdsNS4yNzggOS40MzhjLjU3NiAxLjAzIDEuMzg3IDEuOTEgMi4zNjggMi41NjdsMTQuNjg4IDkuODU0YzIuMDIzIDEuMzU4IDQuNTgyIDEuNjQ3IDYuODU4Ljc3NyA1Ljc0MS0yLjE5NyAxMi4yMDctMS40NzEgMTcuMjk2IDEuOTQzIDQuMzc1IDIuOTM1IDcuMjU4IDcuNDE3IDguMTE2IDEyLjYxOS44NTcgNS4xOTYtLjQ0MyAxMC4zNzctMy42NjMgMTQuNTg3LTUuNzA2IDcuNDYyLTE2LjUwNCA5LjU1LTI0LjU4NyA0Ljc1My00Ljk4Mi0yLjk1Ny04LjMtNy45MzYtOS4xMDEtMTMuNjYtLjI5Ni0yLjExMy0xLjQ3OC00LTMuMjUtNS4xODhsLTE2LjI2OC0xMC45MTRjLS45OC0uNjU4LTIuMTAyLTEuMDc1LTMuMjczLTEuMjE3bC0xMC4yMzktMS4yNDVjLTUuNzI3LS42OTgtMTEuNDM3IDEuMjEzLTE1LjY2MiA1LjI0LTguMiA3LjgxMy0xNy40MDggMTQuNDY4LTI3LjM2NyAxOS43NzktNS43OTIgMy4wODgtOS42NDggOS4xMTQtMTAuMDYyIDE1LjcyNGwtMS4yOTkgMjAuNzQ0Yy0uMDQ4Ljc3My4wMjMgMS41NS4yMTMgMi4zMDFsNi40MzcgMjUuNTQ2Yy42NTcgMi42MDkgMi42NjIgNC42NjYgNS4yNTIgNS4zOSA4LjczNCAyLjQ0MyAxNS41MzQgOS40MTEgMTcuNzQ1IDE4LjE4NCAxLjY4MyA2LjY4LjYxOSAxMy41OS0yLjk5OCAxOS40NTYtMy42MTQgNS44NjMtOS4zMTYgOS45MTgtMTYuMDU1IDExLjQxOS0xMi41NjMgMi44LTI1LjUyNy00LjU3OS0yOS41MDctMTYuNzk2LTMuMDA2LTkuMjI5LS41NzgtMTkuMTc4IDYuMzM1LTI1Ljk2NiAxLjkwMS0xLjg2NiAyLjY2OS00LjYwMSAyLjAxOC03LjE4NGwtNi40NzctMjUuNzAyYy0uMTg5LS43NTEtLjQ5NC0xLjQ2OS0uOTAzLTIuMTI3bC05Ljk4NS0xNi4wNjFjLTQuMDQ3LTYuNTExLTExLjA5Ny0xMC4zOTYtMTguOTM3LTEwLjM5Ni0uMDA1IDAtLjAxIDAtLjAxNiAwLTguMTEgMC0xNi4yMjktLjc5My0yNC4xMzMtMi4zNTctNy41MzQtMS40OTItMTUuMTUzLjkxNC0yMC4zODEgNi40MzFsLTEzLjIwMiAxMy45MzJjLS41MzMuNTYzLS45NzYgMS4yMDQtMS4zMTMgMS45MDJsLTE0Ljc3OCAzMC42NjFjLTEuMDAxIDIuMDc3LS45OTEgNC41LjAyNyA2LjU2OCAyLjU4OSA1LjI2IDIuNjE5IDExLjI5NS4wODIgMTYuNTU5LTIuMjkgNC43NTEtNi4zMjQgOC4yMzktMTEuMzU4IDkuODIyLTUuMDMgMS41ODItMTAuMzQ3IDEuMDE5LTE0Ljk3MS0xLjU4My04LjEzMy00LjU3Ny0xMS43My0xNC45MjktOC4xOS0yMy41NjYgMi4zNjEtNS43NTkgNy4xODUtOS44NjkgMTMuMjM2LTExLjI3NyAyLjIxMS0uNTE0IDQuMDcxLTIuMDAzIDUuMDU3LTQuMDQ4bDE0Ljg3NC0zMC44NThjLjMzNi0uNjk4LjU2My0xLjQ0NC42Ny0yLjIxMWwyLjkxNC0yMC43MTZjLjkwMy02LjQxNy0xLjYzNy0xMi45MTYtNi42MjctMTYuOTYyLTExLjI0LTkuMTE1LTIwLjczLTE5Ljk5Mi0yOC4yMDgtMzIuMzMtMy4yNzYtNS40MDUtOS4yNzYtOC44MDEtMTUuNjYtOC44NjFsLTIwLjY4LS4xOTdjLS43ODktLjAxLTEuNTcxLjEwOS0yLjMyMi4zNDVsLTIyLjY0MiA3LjEyM2MtMi44MzEuODkxLTQuODcxIDMuMzY3LTUuMjAzIDYuMzE2LTEuMzAyIDExLjU3MS05LjMxMiAyMS4zMDMtMjAuNDA3IDI0Ljc5My0xNS4zMDEgNC44MTQtMzEuODA3LTMuNjcyLTM2Ljc4OC0xOC45MTktMS45OC02LjA2MS0xLjkxNi0xMi40OTMuMTg3LTE4LjYwMSAxLjM0OC0zLjkxNy0uNzM0LTguMTg1LTQuNjUxLTkuNTMzLTMuOTE4LTEuMzUtOC4xODUuNzM1LTkuNTMzIDQuNjUxLTMuMTczIDkuMjItMy4yNjQgMTguOTUxLS4yNjIgMjguMTQxIDYuMDk0IDE4LjY1MyAyMy42MDUgMzAuNjA2IDQyLjMyNSAzMC42MDUgNC4zODQgMCA4LjgzOC0uNjU2IDEzLjIyMy0yLjAzNiAxNS4xNS00Ljc2OSAyNi40ODgtMTcuMjMzIDI5Ljk3Ny0zMi40ODZsMTcuMTQ2LTUuMzk0IDE5LjQ5Mi4xODVjMS4yMy4wMTIgMi4zNy42MzkgMi45NzQgMS42MzcgOC4zNzUgMTMuODE5IDE5LjAwMyAyNi4wMDEgMzEuNTg4IDM2LjIwNy45MjcuNzUxIDEuMzk1IDEuOTg2IDEuMjIxIDMuMjIybC0yLjc1MSAxOS41NTktMTIuOTIzIDI2LjgxM2MtOS4wNSAzLjIwMy0xNi40MTYgMTAuMTUtMjAuMTAyIDE5LjE0MS02LjQ2NiAxNS43NzItLjE0MSAzMy45NyAxNC43MTMgNDIuMzI4IDguMjcyIDQuNjU2IDE3LjgwMSA1LjY1OCAyNi44MjUgMi44MiA4Ljg5OC0yLjc5NyAxNi4zMjQtOS4yMTkgMjAuMzczLTE3LjYxOSAzLjk5Ni04LjI5MSA0LjQ0Ni0xNy42NDEgMS4zNjEtMjYuMTc3bDEyLjgwMS0yNi41NTggMTIuMzk4LTEzLjA4M2MxLjY2Ny0xLjc2IDQuMTI2LTIuNTIgNi41ODEtMi4wMzQgOC44NiAxLjc1MyAxNy45NTkgMi42NDMgMjcuMTMyIDIuNjQzaC4wMDZjMi41NDIgMCA0LjgzMSAxLjIzOSA2LjEyMSAzLjMxNmw5LjM2OCAxNS4wNjggNS4yMDggMjAuNjY3Yy04LjY3MiAxMC41ODQtMTEuNDA1IDI0Ljc3Ny03LjA4OCAzOC4wMzIgNS40NCAxNi43MDMgMjEuMzk3IDI3LjczNiAzOC40NTkgMjcuNzM1IDIuODQgMCA1LjcxMy0uMzA2IDguNTcxLS45NDIgMTAuNzI2LTIuMzg5IDE5LjgwNS04Ljg0OCAyNS41NjMtMTguMTg5IDUuNzYtOS4zNDQgNy40NTUtMjAuMzUxIDQuNzczLTMwLjk5Mi0zLjE1My0xMi41MTQtMTIuMTYtMjIuNzItMjMuOTg5LTI3LjUxMWwtNS4xNTktMjAuNDc2IDEuMjI2LTE5LjU3NmMuMDkxLTEuNDU1LjkxNS0yLjc2OCAyLjE0OS0zLjQyNiAxMS4xNTgtNS45NTEgMjEuNDcyLTEzLjQwNSAzMC42NTctMjIuMTU1Ljk2Ny0uOTIxIDIuMjQ0LTEuMzYgMy41MDQtMS4yMDhsOC40NTcgMS4wMjggMTIuMTkyIDguMThjMi4xNjUgOC43OTQgNy43MyAxNi4zMzMgMTUuNjM2IDIxLjAyNSAxNC43NTUgOC43NTUgMzMuNzM4IDUuMDg0IDQ0LjE1Ny04LjU0MiA1Ljc2MS03LjUzMyA4LjA4Ni0xNi44MTYgNi41NDctMjYuMTQtMS41MTctOS4xOTQtNi44MjQtMTcuNDQ0LTE0LjU2LTIyLjYzNC03Ljk5OS01LjM2Ni0xNy44ODctNy4wMzMtMjcuMTM0LTQuNzE5bC0xMC4yMTYtNi44NTQtNC40MDEtNy44N2MtLjUyMy0uOTM2LS41OTEtMi4wNzUtLjE4Ny0zLjEyNSAzLjc4Mi05LjgyMSA2LjQzOS0yMC4wOTIgNy44OTctMzAuNTI3LjE3OC0xLjI3NS45MzUtMi4zNSAyLjAyMy0yLjg3NWwxMy41ODUtNi41NTQgMTMuMzM3LTEuMTc4YzkuMjcgMTUuMzQzIDI1LjgxMSAyMy45MzEgNDMuNDc5IDIyLjE1OCAzLjI2MS0uMzI3IDYuNTA2LTEuMDE3IDkuNjQ3LTIuMDUgMy45MzUtMS4yOTUgNi4wNzQtNS41MzQgNC43NzktOS40NjktMS4yOTUtMy45MzgtNS41MzUtNi4wNzYtOS40NjktNC43ODJ6IiBkYXRhLW9yaWdpbmFsPSIjMDAwMDAwIiBjbGFzcz0iaG92ZXJlZC1wYXRoIGFjdGl2ZS1wYXRoIiBkYXRhLW9sZF9jb2xvcj0iIzAwMDAwMCIgZmlsbD0iIzJDMkMyQyIvPjxwYXRoIGQ9Im0yNzAuNTQ0IDE0OS4yNmMtNy44NjMtMS44NDctMTUuOTctMi43ODMtMjQuMDk2LTIuNzgzLTU3LjkwNiAwLTEwNS4wMTcgNDcuMTEtMTA1LjAxNyAxMDUuMDE3czQ3LjExIDEwNS4wMTcgMTA1LjAxNyAxMDUuMDE3IDEwNS4wMTctNDcuMTEgMTA1LjAxNy0xMDUuMDE3YzAtMzYuMjA2LTE4LjI4OS02OS4zOTctNDguOTIyLTg4Ljc4OS0zLjUwMS0yLjIxNi04LjEzNS0xLjE3NC0xMC4zNDkgMi4zMjUtMi4yMTYgMy41LTEuMTc0IDguMTMzIDIuMzI2IDEwLjM0OSAyNi4yNjUgMTYuNjI2IDQxLjk0NSA0NS4wODEgNDEuOTQ1IDc2LjExNSAwIDQ5LjYzNS00MC4zODEgOTAuMDE3LTkwLjAxNyA5MC4wMTdzLTkwLjAxNy00MC4zODEtOTAuMDE3LTkwLjAxNyA0MC4zODEtOTAuMDE3IDkwLjAxNy05MC4wMTdjNi45NzMgMCAxMy45MjYuODAzIDIwLjY2NyAyLjM4NiA0LjAzMy45NSA4LjA2OS0xLjU1NCA5LjAxNi01LjU4Ni45NDctNC4wMzMtMS41NTUtOC4wNjktNS41ODctOS4wMTd6IiBkYXRhLW9yaWdpbmFsPSIjMDAwMDAwIiBjbGFzcz0iaG92ZXJlZC1wYXRoIGFjdGl2ZS1wYXRoIiBkYXRhLW9sZF9jb2xvcj0iIzAwMDAwMCIgZmlsbD0iIzJDMkMyQyIvPjxwYXRoIGQ9Im04Mi4zNjUgODEuNzAzYzE5LjQ0OSAwIDM1LjI3MS0xNS44MjIgMzUuMjcxLTM1LjI3MXMtMTUuODIyLTM1LjI3MS0zNS4yNzEtMzUuMjcxLTM1LjI3MSAxNS44MjMtMzUuMjcxIDM1LjI3MmMwIDE5LjQ0OCAxNS44MjIgMzUuMjcgMzUuMjcxIDM1LjI3em0wLTU1LjU0MWMxMS4xNzcgMCAyMC4yNzEgOS4wOTQgMjAuMjcxIDIwLjI3MXMtOS4wOTQgMjAuMjcxLTIwLjI3MSAyMC4yNzEtMjAuMjcxLTkuMDk0LTIwLjI3MS0yMC4yNzEgOS4wOTQtMjAuMjcxIDIwLjI3MS0yMC4yNzF6IiBkYXRhLW9yaWdpbmFsPSIjMDAwMDAwIiBjbGFzcz0iaG92ZXJlZC1wYXRoIGFjdGl2ZS1wYXRoIiBkYXRhLW9sZF9jb2xvcj0iIzAwMDAwMCIgZmlsbD0iIzJDMkMyQyIvPjxwYXRoIGQ9Im03NC43NzQgMzg2LjY1M2MtMTQuMjUgMC0yNS44NDQgMTEuNTkzLTI1Ljg0NCAyNS44NDNzMTEuNTkzIDI1Ljg0NCAyNS44NDQgMjUuODQ0YzE0LjI1IDAgMjUuODQzLTExLjU5MyAyNS44NDMtMjUuODQ0IDAtMTQuMjQ5LTExLjU5My0yNS44NDMtMjUuODQzLTI1Ljg0M3ptMCAzNi42ODdjLTUuOTc5IDAtMTAuODQ0LTQuODY0LTEwLjg0NC0xMC44NDQgMC01Ljk3OSA0Ljg2NC0xMC44NDMgMTAuODQ0LTEwLjg0MyA1Ljk3OSAwIDEwLjg0MyA0Ljg2NCAxMC44NDMgMTAuODQzIDAgNS45OC00Ljg2NCAxMC44NDQtMTAuODQzIDEwLjg0NHoiIGRhdGEtb3JpZ2luYWw9IiMwMDAwMDAiIGNsYXNzPSJob3ZlcmVkLXBhdGggYWN0aXZlLXBhdGgiIGRhdGEtb2xkX2NvbG9yPSIjMDAwMDAwIiBmaWxsPSIjMkMyQzJDIi8+PHBhdGggZD0ibTQxMS41NjMgNDA4LjExMWMtMTkuMDMzIDAtMzQuNTE4IDE1LjQ4NC0zNC41MTggMzQuNTE4czE1LjQ4NCAzNC41MTggMzQuNTE4IDM0LjUxOCAzNC41MTgtMTUuNDg0IDM0LjUxOC0zNC41MTgtMTUuNDg1LTM0LjUxOC0zNC41MTgtMzQuNTE4em0wIDU0LjAzNWMtMTAuNzYyIDAtMTkuNTE4LTguNzU1LTE5LjUxOC0xOS41MThzOC43NTUtMTkuNTE4IDE5LjUxOC0xOS41MTggMTkuNTE4IDguNzU1IDE5LjUxOCAxOS41MTgtOC43NTYgMTkuNTE4LTE5LjUxOCAxOS41MTh6IiBkYXRhLW9yaWdpbmFsPSIjMDAwMDAwIiBjbGFzcz0iaG92ZXJlZC1wYXRoIGFjdGl2ZS1wYXRoIiBkYXRhLW9sZF9jb2xvcj0iIzAwMDAwMCIgZmlsbD0iIzJDMkMyQyIvPjxwYXRoIGQ9Im0yNDkuMjAzIDIyNC42MDFjMC0xOC4wMjUtMTQuNjY0LTMyLjY4OS0zMi42ODktMzIuNjg5cy0zMi42ODkgMTQuNjY0LTMyLjY4OSAzMi42ODkgMTQuNjY0IDMyLjY4OSAzMi42ODkgMzIuNjg5IDMyLjY4OS0xNC42NjUgMzIuNjg5LTMyLjY4OXptLTMyLjY4OSAxNy42ODljLTkuNzU0IDAtMTcuNjg5LTcuOTM1LTE3LjY4OS0xNy42ODlzNy45MzUtMTcuNjg5IDE3LjY4OS0xNy42ODkgMTcuNjg5IDcuOTM1IDE3LjY4OSAxNy42ODktNy45MzUgMTcuNjg5LTE3LjY4OSAxNy42ODl6IiBkYXRhLW9yaWdpbmFsPSIjMDAwMDAwIiBjbGFzcz0iaG92ZXJlZC1wYXRoIGFjdGl2ZS1wYXRoIiBkYXRhLW9sZF9jb2xvcj0iIzAwMDAwMCIgZmlsbD0iIzJDMkMyQyIvPjxwYXRoIGQ9Im0yMzYuNzM1IDMyNy40ODNjMTMuNTg5IDAgMjQuNjQ1LTExLjA1NiAyNC42NDUtMjQuNjQ1cy0xMS4wNTYtMjQuNjQ1LTI0LjY0NS0yNC42NDUtMjQuNjQ1IDExLjA1Ni0yNC42NDUgMjQuNjQ1YzAgMTMuNTkgMTEuMDU1IDI0LjY0NSAyNC42NDUgMjQuNjQ1em0wLTM0LjI4OWM1LjMxOCAwIDkuNjQ1IDQuMzI3IDkuNjQ1IDkuNjQ1cy00LjMyNyA5LjY0NS05LjY0NSA5LjY0NS05LjY0NS00LjMyNy05LjY0NS05LjY0NSA0LjMyNi05LjY0NSA5LjY0NS05LjY0NXoiIGRhdGEtb3JpZ2luYWw9IiMwMDAwMDAiIGNsYXNzPSJob3ZlcmVkLXBhdGggYWN0aXZlLXBhdGgiIGRhdGEtb2xkX2NvbG9yPSIjMDAwMDAwIiBmaWxsPSIjMkMyQzJDIi8+PHBhdGggZD0ibTI2OC42NzMgMjUxLjQ5NGMwIDEyLjA3NCA5LjgyMyAyMS44OTcgMjEuODk3IDIxLjg5N3MyMS44OTctOS44MjMgMjEuODk3LTIxLjg5Ny05LjgyMy0yMS44OTctMjEuODk3LTIxLjg5Ny0yMS44OTcgOS44MjItMjEuODk3IDIxLjg5N3ptMjEuODk3LTYuODk4YzMuODAzIDAgNi44OTcgMy4wOTQgNi44OTcgNi44OTdzLTMuMDk0IDYuODk3LTYuODk3IDYuODk3LTYuODk3LTMuMDk0LTYuODk3LTYuODk3IDMuMDkzLTYuODk3IDYuODk3LTYuODk3eiIgZGF0YS1vcmlnaW5hbD0iIzAwMDAwMCIgY2xhc3M9ImhvdmVyZWQtcGF0aCBhY3RpdmUtcGF0aCIgZGF0YS1vbGRfY29sb3I9IiMwMDAwMDAiIGZpbGw9IiMyQzJDMkMiLz48L2c+PC9nPiA8L3N2Zz4K"
    let ChartDisease = echarts.init(document.getElementById('chartDisease'));
    ChartDisease.showLoading();
    ChartDisease.hideLoading();
    let option = {
      tooltip: {
        trigger: 'item',
        showDelay: 0,
        transitionDuration: 0.2,
        formatter: "{a} <br/>{b} : {c} ({d}%)",
        backgroundColor: '#FFFFFF',
        padding: 5,
        textStyle: {
          color: '#212121',
          fontSize: 13,
          lineHeight:10,
          fontWeight: 'bold',
          fontFamily: 'Helvetica-Light'
        },
        extraCssText: 'box-shadow: 0 0 3px rgba(0, 0, 0, 0.3);'
      },
      graphic: {
          elements: [{
              type: 'image',
              style: {
                  image: chartIconDisease,
                  width: 80,
                  height: 80
              },
              left: 'center',
              top: 'center'
          }]
      },
      animation: true,
      animationThreshold: 2000,
      animationDuration: 1800,
      //animationEasing: 'quinticInOut',
      animationDelay: 0,
      animationDurationUpdate: 400,
      //animationEasingUpdate: 'quinticInOut',
      animationDelayUpdate: 0,
      legend: {
          orient: 'horizontal',
          top: 12,
          x: 'center',
          width: '100%',
          textStyle: {
              color: '#212121',
              fontWeight: 'bold',
              fontFamily: 'Helvetica-Light'
          },
          data: this.labelGrafica
      },
      toolbox: {
        show: true,
        orient: 'vertical',
        right: 12,
        top: 8,
        itemGap: 5,
        feature: {
            dataView: {
              show: false,
              readOnly: false
            },
            restore: {
              show: false,
              title: 'Reiniciar Grafica',
              iconStyle: {
                borderColor: '#212121',
                borderWidth: 1
              },
              emphasis: {
                iconStyle: {
                  borderColor: '#109BF8',
                  borderWidth: 1,
                  textFill: '#109BF8',
                  textPadding: 5
                }
              }
            },
            saveAsImage: {
              show: false,
              title: '-',
              iconStyle: {
                borderColor: '#212121',
                borderWidth: 1
              },
              emphasis: {
                iconStyle: {
                  borderColor: '#109BF8',
                  borderWidth: 1,
                  textFill: '#109BF8',
                  textPadding: 5
                }
              }
            }
        }
      },
      textStyle: {
        fontWeight: 'bold',
        fontFamily: 'Helvetica-Light'
      },
      color: ['#020175','#184A9C','#2F91C2','#46DAEB'],
      calculable : true,
      series: [{
          name:'Tipo de Enfermedad',
          type: 'pie',
          radius: ['38%', '55%'],
          label: {
              normal: {
                  show: true,
                  fontSize: 12,
                  fontWeight: 'bold',
                  fontFamily: 'Helvetica-Light',
                  formatter: '{c|{c}}\n{hr|}\n{d|{d}%}',
                  rich: {
                      b: {
                        fontSize: 12,
                        color: '#212121',
                        align: 'left',
                        padding: 3
                      },
                      hr: {
                        borderColor: '#212121',
                        width: '100%',
                        borderWidth: 2,
                        height: 0
                      },
                      d: {
                        fontSize: 12,
                        color: '#212121',
                        align: 'left',
                        padding: 3
                      },
                      c: {
                        fontSize: 12,
                        color: '#212121',
                        align: 'left',
                        padding: 3
                      }
                  }
              },
              emphasis: {
                show: true,
                fontSize: 12,
                fontWeight: 'bold',
                fontFamily: 'Helvetica-Light'
              }
          },
          labelLine: {
            normal: {
              show: true,
              length: 12,
              length2: 12,
              lineStyle: {
                  color: '#212121',
                  width: 2
              }
            }
          },
          itemStyle: {
            normal: {
              opacity: 1,
              //shadowBlur: 5,
              shadowOffsetX: 0,
              shadowOffsetY: 0,
              //shadowColor: 'rgba(33, 33, 33, 0.2)'
            }
          },
          data: this.dataGrafica
      }]
  };

    ChartDisease.setOption(option);

    $(window).on('resize', function(){
        if(ChartDisease != null && ChartDisease != undefined){
            ChartDisease.resize();
        }
    });

  }

}


