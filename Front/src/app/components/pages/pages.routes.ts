// Module Angular
import { NgModule, Component } from '@angular/core';

// Router Module Angular
import { Routes, RouterModule } from '@angular/router';

// Components
import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UsermanagementComponent } from './usermanagement/usermanagement.component';
import { UseractivityComponent } from './useractivity/useractivity.component';

// Guards
import { LoginRequiredGuard } from '../../services/guards/login-required.guard';
import { HmofilterRequiredGuard } from '../../services/guards/hmofilter-required.guard';
import { RolVerifyGuard } from '../../services/guards/rol-verify.guard';


const pagesRoutes: Routes = [
  {
  path: '', component: PagesComponent,
  children: [
    {path: 'dashboard', component: DashboardComponent, canActivate: [ LoginRequiredGuard, HmofilterRequiredGuard ]},
    {path: 'userManagement', component: UsermanagementComponent, canActivate: [ LoginRequiredGuard, HmofilterRequiredGuard, RolVerifyGuard ]},
    {path: 'userActivity', component: UseractivityComponent, canActivate: [ LoginRequiredGuard, HmofilterRequiredGuard, RolVerifyGuard ]},
    {path: '', redirectTo: '/dashboard', pathMatch: 'full', canActivate: [ LoginRequiredGuard, HmofilterRequiredGuard ]}
  ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(pagesRoutes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
