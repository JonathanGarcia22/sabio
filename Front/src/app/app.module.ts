// Modules Angular
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// Modules libs
import { NgwWowModule } from 'ngx-wow';

// Modules shared
import { PagesModule } from './components/pages/pages.module';
import { SharedModule } from './components/shared/shared.module';

// Routs
import { AppRoutingModule } from './app-routing.module';

// Golbal Component
import { AppComponent } from './components/app/app.component';

// Components
import { LandingComponent } from './components/landing/landing.component';
import { AboutComponent } from './components/landing/about.component';
import { LoginComponent } from './components/landing/login.component';
import { HmofilterComponent } from './components/landing/hmofilter.component';
import { ChangepasswordComponent } from './components/landing/changepassword.component';
import { ForgotpasswordComponent } from './components/landing/forgotpassword.component';
import { ResponseResetPasswordComponent } from './components/landing/response-reset-password.component';

// Servicios
import { ServiceModule } from './services/service.module';

import { FormsModule } from '@angular/forms';
import { BnNgIdleService } from 'bn-ng-idle';

@NgModule({
  declarations: [
    AppComponent,
    LandingComponent,
    AboutComponent,
    LoginComponent,
    HmofilterComponent,
    ChangepasswordComponent,
    ForgotpasswordComponent,
    ResponseResetPasswordComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    PagesModule,
    SharedModule,
    NgwWowModule,
    FormsModule,
    ServiceModule
  ],
  exports: [
    SharedModule
  ],
  providers: [BnNgIdleService],
  bootstrap: [AppComponent]
})
export class AppModule { }
