import { TestBed, async, inject } from '@angular/core/testing';

import { ProtectedLoginGuard } from './protected-login.guard';

describe('ProtectedLoginGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProtectedLoginGuard]
    });
  });

  it('should ...', inject([ProtectedLoginGuard], (guard: ProtectedLoginGuard) => {
    expect(guard).toBeTruthy();
  }));
});
