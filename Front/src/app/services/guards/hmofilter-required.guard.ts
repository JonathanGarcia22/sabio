import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { FiltroBoardcardsService } from 'src/app/components/shared/filtro-boardcards.service';

@Injectable({
  providedIn: 'root'
})
export class HmofilterRequiredGuard implements CanActivate {

  constructor(
    public router: Router, public _actualizarFiltros: FiltroBoardcardsService
  ) {}
  
  canActivate(){
    if (sessionStorage.getItem("enfermedad") != null ){
      return true;
    }
    else{
      console.log("Debe seleccionar los filtros para continuar");
      this.router.navigate(['/hmoFilter']);
      return false;
    }
  }
}
