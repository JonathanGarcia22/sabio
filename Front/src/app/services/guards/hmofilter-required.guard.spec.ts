import { TestBed, async, inject } from '@angular/core/testing';

import { HmofilterRequiredGuard } from './hmofilter-required.guard';

describe('HmofilterRequiredGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HmofilterRequiredGuard]
    });
  });

  it('should ...', inject([HmofilterRequiredGuard], (guard: HmofilterRequiredGuard) => {
    expect(guard).toBeTruthy();
  }));
});
