import { TestBed, async, inject } from '@angular/core/testing';

import { RolVerifyGuard } from './rol-verify.guard';

describe('RolVerifyGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RolVerifyGuard]
    });
  });

  it('should ...', inject([RolVerifyGuard], (guard: RolVerifyGuard) => {
    expect(guard).toBeTruthy();
  }));
});
