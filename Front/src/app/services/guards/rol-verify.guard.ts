import { Injectable } from '@angular/core';
import { CanActivate, Router, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { UsuarioService } from 'src/app/services/usuario/usuario.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class RolVerifyGuard implements CanActivate {

  constructor(
    public router: Router, public _usuarioService: UsuarioService
  ) {}
  
  canActivate(){
    if ( this._usuarioService.usuario.rol === 'ADMIN' ) {
      console.log("Permiso para acceder a secciones de administración");
      return true;
    }
    else{
      console.log("El usuario NO tiene permisos para ingresar a esta sección");
      this.router.navigate(['/dashboard']);
      return false;
    }
  }
}
