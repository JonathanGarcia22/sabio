import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { UsuarioService } from '../usuario/usuario.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProtectedLoginGuard implements CanActivate {

  constructor(
    public _usuarioService: UsuarioService,
    public router: Router
  ) {}

  canActivate(){
    
    if ( this._usuarioService.estaLogueado() ) {
      console.log("Usuario logeado " + this._usuarioService.estaLogueado())
      this.router.navigate(['/hmoFilter']);
      return false;
    } else {
      console.log( 'Inicio de sesión' );
      return true;
    }

  }
  
}
