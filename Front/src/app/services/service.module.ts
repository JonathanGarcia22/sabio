import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsuarioService } from './service.index';
import { LoginComponent } from '../components/landing/login.component';
import { LoginRequiredGuard } from './guards/login-required.guard';
import { ProtectedLoginGuard } from './guards/protected-login.guard';
import { HmofilterRequiredGuard } from './guards/hmofilter-required.guard';
import { RolVerifyGuard } from './guards/rol-verify.guard';


@NgModule({
  imports: [
    CommonModule,
    HttpClientModule
  ],
  providers: [
    UsuarioService,
    LoginRequiredGuard,
    ProtectedLoginGuard,
    HmofilterRequiredGuard,
    RolVerifyGuard
  ],
  declarations: []
})
export class ServiceModule { }
