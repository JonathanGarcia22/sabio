import { Injectable } from '@angular/core';
import { Usuario } from '../../models/usuario.model';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { URL_SERVICIOS } from '../../config/config';
import { map } from 'rxjs/internal/operators/map';
import { throwError, BehaviorSubject } from 'rxjs'
import { catchError } from 'rxjs/operators';
import { BnNgIdleService } from 'bn-ng-idle';

import Swal from 'sweetalert2';


@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  usuario: Usuario;
  token: string;
  refresh: string;
  username: string;
  email: string;
  firstsesson:boolean;
  
  //comunicacion entre components que no son padre - hijo
  //private enfermedad = new BehaviorSubject<string>('En espera de seleccionar una enfermedad');
  //public customMessage = this.enfermedad.asObservable();


  constructor(
    public http: HttpClient,
    public router: Router,
    private bnIdle: BnNgIdleService
  ) {
    this.cargarStorage();
  }


  estaLogueado() {
    return ( this.token.length > 5 ) ? true : false;
  }

  seguimientoInactividad(){
      if ( this.estaLogueado() ){   
          this.bnIdle.startWatching(600).subscribe((res) => {

            if(res) {
      
                this.logout("Recargar")
            }
          })
      }
  }

  datosRealesUsuario(){
    
  }

  validaToken( token:string ){
    
    let url = URL_SERVICIOS + '/api/login/token/verify/';

    let body = { token:token }

    return this.http.post( url, body ).pipe(
      map( (resp: any) => {
        return resp;
      }),
      catchError( err => {
        url =  URL_SERVICIOS + '/api/login/token-refresh/';
        let refresh:string = String(sessionStorage.getItem('refresh'));
        let bodyRefresh = { refresh:refresh };

        return this.http.post( url, bodyRefresh ).pipe(
          map( (resp:any) => {
            let nuevoToken:string = String(Object.values(resp)[0]);
            sessionStorage.setItem('token', nuevoToken);
            return nuevoToken;
          }),
          catchError( err => {
            Swal.fire({
              icon: 'error',
              title: 'Error',
              text: "Sus credenciales de acceso son inválidas o han expirado",
              allowOutsideClick: false
            });
        
            this.logout("")

            return throwError( err );
          }));
      }));
  }

  cargarStorage() {

    if ( sessionStorage.getItem('token')) {
      this.token = sessionStorage.getItem('token');
      this.usuario = JSON.parse( sessionStorage.getItem('usuario') );
    } else {
      this.token = '';
      this.usuario = null;
    }
  }


  guardarStorage( id: string, token: string, refresh: string, username: string, first_session: boolean, rol: string, entidad: string) {

    this.token = token;
    this.refresh = refresh;
    this.username = username
    this.usuario = new Usuario(username,null,null,null,rol,entidad,first_session,null,id)

    sessionStorage.setItem('token', token );
    sessionStorage.setItem('refresh', refresh );
    sessionStorage.setItem('usuario', JSON.stringify(this.usuario))
    sessionStorage.setItem('username', username)

  }

  login( usuario: Usuario ) {

    let url = URL_SERVICIOS + '/api/login/';
  
    return this.http.post( url, usuario ).pipe(
                map( (resp: any) => {

                  if ( typeof(resp.message) == "undefined" ){
                    this.guardarStorage( resp.id, resp.access, resp.refresh, resp.username, resp.first_session, resp.rol, resp.entidad);
                    return resp;
                  }
                  else{
                    Swal.fire({
                      icon: 'error',
                      title: 'Error de logueo',
                      text: resp.message,
                      allowOutsideClick: false
                    });
                  }
                  
                }),
                //Credenciales inválidas
                catchError( err => {
                  Swal.fire({
                    icon: 'error',
                    title: 'Error de logueo',
                    text: err.error.Error,
                    allowOutsideClick: false
                  });

                  return throwError( err );
                }));

  }

  logout( mensaje:string ) {
    this.usuario = null;
    this.token = '';
    this.refresh = '';
    this.username = '';

    sessionStorage.removeItem('token');
    sessionStorage.removeItem('refresh');
    sessionStorage.removeItem('usuario');
    sessionStorage.removeItem('username');
    sessionStorage.removeItem('enfermedad');
    sessionStorage.removeItem('entidad');
    sessionStorage.removeItem('departamento');
    sessionStorage.removeItem('listaEnfermedades');
    sessionStorage.removeItem('listaEntidades');
    sessionStorage.removeItem('listaDepartamentos');
    sessionStorage.removeItem('enfermedadesDepartamentos');

    this.router.navigate(['/login']);

    if ( mensaje == "Recargar" ){
      location.reload();
    }
  }

  cambiarContrasena( body ){
    let url = URL_SERVICIOS + '/api/login/change_password/';
    
    return this.http.post( url, body ).pipe(
                map( (resp: any) => {

                  return resp;
                }),
                catchError( err => {
                  Swal.fire({
                    icon: 'error',
                    title: 'Error',
                    text: err.error.error,
                    allowOutsideClick: false
                  });
          
                  return throwError( err );
                }));
  }

  resetContrasena( email ){

    let body = {email: email}

    let url = URL_SERVICIOS + '/api/login/reset_password/';

    return this.http.post( url, body ).pipe(
                map( (resp: any) => {

                  if ( typeof(resp.message) == "undefined" ){
  
                    Swal.fire({
                      icon: 'success',
                      title: 'Correo enviado con éxito',
                      text: 'Favor revise su correo electrónico',
                      allowOutsideClick: false
                    });
  
                    return resp;
                  }
                  else{
                    Swal.fire({
                      icon: 'error',
                      title: 'Error',
                      text: resp.message,
                      allowOutsideClick: false
                    }).then((result) => {
                      if (result.value) {
                        this.logout("Recargar");
                      }
                    });
                  }
                }),
                catchError( err => {

                  Swal.fire({
                    icon: 'error',
                    title: 'Error',
                    text: err.error.error,
                    allowOutsideClick: false
                  }).then((result) => {
                    if (result.value) {
                      this.logout("Recargar");
                    }
                  });

                  return throwError( err );
                }));
            }

  responseResetContrasena( uidb64, token ){
    
    let uidb64_copy = uidb64.split('\\')
    uidb64_copy = uidb64_copy[0] + "%5C" + uidb64_copy[1]
    uidb64 = uidb64_copy

    let url = `${ URL_SERVICIOS }/api/login/response_reset_password/${ uidb64 }/${ token }`;
    
    return this.http.get( url ).pipe(
                map( (resp: any) => {

                  return resp;
                }),
                catchError( err => {
 
                  Swal.fire({
                    icon: 'error',
                    title: 'Error',
                    text: err.error.error,
                    allowOutsideClick: false
                  });

                  this.router.navigate(['../landing'])
          
                  return throwError( err );
                }));
            }
  
  listarEntidadesPorUsuario( id ){
    let url = URL_SERVICIOS + '/api/resultados/list_entities_par_user/';
    let body = { id:id }

    return this.http.post( url,body ).pipe(
      map( (resp: any) => {

        return resp;
      }),
      catchError( err => {

        Swal.fire({
          icon: 'error',
          title: 'Error',
          text: "Problema al cargar las entidades",
          allowOutsideClick: false
        });


        this.logout("");

        return throwError( err );
      }));
  }

  cargarEnfermedadesDepartamentos( entidad )   {
    let url = URL_SERVICIOS + '/api/resultados/list_diseases_places/';

    let body = { entidad:entidad }

    return this.http.post( url, body ).pipe(
      map( (resp: any) => {

        return resp;
      }),
      catchError( err => {

        Swal.fire({
          icon: 'error',
          title: 'Error',
          text: err.error.error,
          allowOutsideClick: false
        });

        return throwError( err );
      }));
  }
    
  verificarUsuarioEntidad( id:string, entidad:string ){
    let url = URL_SERVICIOS + '/api/resultados/user_entity_permission/';
    let body = { id:id, entidad:entidad }

    return this.http.post( url, body ).pipe(
      map( (resp: any) => {

        return resp;
      }),
      catchError( err => {

        Swal.fire({
          icon: 'error',
          title: 'Error',
          text: "Error en la cosulta de información",
          allowOutsideClick: false
        });

        return throwError( err );
      }));
  }

  //comunicacion entre components que no son padre - hijo
  //enviarEnfermedadSeleccionada( enfermedad:string ): void{
    //this.enfermedad.next(enfermedad)
  //}

  cargarResultados( collection, ubicacion, cantidad )   {
    let url = URL_SERVICIOS + '/api/resultados/bring_result/';

    let body = { collection:collection, ubicacion:ubicacion, cantidad:cantidad }

    return this.http.post( url, body ).pipe(
      map( (resp: any) => {

        return resp;
      }),
      catchError( err => {

        Swal.fire({
          icon: 'error',
          title: 'Error',
          text: err.error.error,
          allowOutsideClick: false
        });

        this.router.navigate(['../login']);

        return throwError( err );
      }));
  }

  listarUsuarios(){
    let url = URL_SERVICIOS + '/api/usuarios/list_user/';
    return this.http.get( url ).pipe(
      map( (resp: any) => {

        return resp;
      }),
      catchError( err => {

        Swal.fire({
          icon: 'error',
          title: 'Error',
          text: err.error.error,
          allowOutsideClick: false
        });

        return throwError( err );
      }));
  }

  guardarUsuario( usuario: Usuario, idResponsable:string ) {

    let body = {'id':idResponsable, 'usuario':usuario}
    
    if ( typeof( usuario.id ) != 'undefined'){
      console.log("Actualizando usuario");

      let url = URL_SERVICIOS + '/api/usuarios/update_user/';

    return this.http.put( url, body ).pipe(
      map( (resp: any) => {

        return resp;
      }),
      catchError( err => {
        
        let mensaje;
        console.log(err);

        Swal.fire({
          icon: 'error',
          title: 'Error',
          text: Object.values(err.error.error)[0][0],
          allowOutsideClick: false
        });

        return throwError( err );
      }));
    }
    else{
      console.log("Creando usuario");

      let url = URL_SERVICIOS + '/api/usuarios/create_user/';

    return this.http.post( url, body ).pipe(
      map( (resp: any) => {

        return resp;
      }),
      catchError( err => {

        console.log(err);
        
        Swal.fire({
          icon: 'error',
          title: 'Error',
          text: Object.values(err.error.error)[0][0],
          allowOutsideClick: false
        });

        return throwError( err );
      }));
    }
  }

  rolForId( id:string ){
    let url = URL_SERVICIOS + '/api/usuarios/rol_for_id/';

    let body = {"id":id}

    return this.http.post( url, body ).pipe(
      map( (resp: any) => {

        return resp;
      }),
      catchError( err => {

        Swal.fire({
          icon: 'error',
          title: 'Error',
          text: err.error.error,
          allowOutsideClick: false
        });
        
        this.logout("");

        return throwError( err );
      }));
  }

  descargarResultados( id:string, collection:string, arrayIds:string[] ){
    let url = URL_SERVICIOS + '/api/resultados/send_result/';

    let body = {"id":id, "collection":collection, "ids":arrayIds}

    Swal.fire({
      icon: 'success',
      title: 'Procesando' ,
      text: "La información será enviada al correo registrado en unos minutos",
      allowOutsideClick: false
    }); 

    return this.http.post( url, body ).pipe(
      map( (resp: any) => {
        return resp;
      }),
      catchError( err => {

        Swal.fire({
          icon: 'error',
          title: 'Error',
          text: err.error.error,
          allowOutsideClick: false
        });

        return throwError( err );
      }));
  }

  actualizarEstado( idresponsable, idpaciente, estado, collection ){
    let url = URL_SERVICIOS + '/api/resultados/update_state/';
    let body = {idresponsable:idresponsable, idpaciente:idpaciente, estado:estado, collection:collection}

    return this.http.post( url, body ).pipe(
      map( (resp: any) => {
        return resp;
      }),
      catchError( err => {

        Swal.fire({
          icon: 'error',
          title: 'Error',
          text: err.error.error,
          allowOutsideClick: false
        });

        return throwError( err );
      }));
  }

  listarLogsDescargas(){
    let url = URL_SERVICIOS + '/api/resultados/data_logs_downloads/';

    return this.http.get( url ).pipe(
      map( (resp: any) => {
        return resp;
      }),
      catchError( err => {

        Swal.fire({
          icon: 'error',
          title: 'Error',
          text: 'Problema en la lectura de los archivos',
          allowOutsideClick: false
        });

        return throwError( err );
      }));
  }

  listarArchivosBorrar(){
    let url = URL_SERVICIOS + '/api/resultados/name_to_delete/';

    return this.http.get( url ).pipe(
      map( (resp: any) => {
        return resp;
      }),
      catchError( err => {

        Swal.fire({
          icon: 'error',
          title: 'Error',
          text: 'Problema en la lectura de los archivos',
          allowOutsideClick: false
        });

        return throwError( err );
      }));
  }

  borrarLogsDescargas( id ){
    let url = URL_SERVICIOS + '/api/resultados/delete_txt/';

    let body = {id:id};

    return this.http.post( url, body ).pipe(
      map( (resp: any) => {
        return resp;
      }),
      catchError( err => {

        Swal.fire({
          icon: 'error',
          title: 'Error',
          text: err.error.error,
          allowOutsideClick: false
        });

        return throwError( err );
      }));
  }

}
